## Conception d''une base de données 5/5 :  un peu de rétro-ingénierie

  note "Supports complémentaires"

-   [Diapositives :
    rétroingénierie](http://sql.bdpedia.fr/files/slretroing.pdf)
-   [Vidéo sur la rétro
    ingénierie](https://mediaserver.cnam.fr/videos/retroingenierie/)

Pour bien comprendre le rapport entre un schéma entité-association et le
schéma relationnel équivalent, il est intéressant de faire le chemin à
l'envers, en partant d'un schéma relationnel connu et en
reconstruisant la modélisation entité-association. Cette courte section
présente cette démarche de *rétro-ingénierie* sur un de nos exemples.

### Quelle conception pour la base des immeubles ?

Reprenons le schéma de la base des immeubles et cherchons à comprendre
comment il est conçu. Ce schéma (relationnel) est le suivant.

-   Immeuble (**id**, nom, adresse)
-   Appart (**id** , no , surface , niveau , *idImmeuble*)
-   Personne (**id**, prénom , nom , profession , *idAppart*)
-   Propriétaire (**idPersonne , idAppart**, quotePart)

Pour déterminer le schéma E/A, commençons par trouver les types
d'entité. Une entité se caractérise par un identifiant qui lui est
propre et la rend indépendante. Trois types d'entité apparaissent
clairement sur notre schéma : les immeubles, les appartements et les
personnes.

Il reste donc la table *Propriétaire* dont l'identifiant est une paire
constituée de l'identifiant d'un immeuble et de l'identifiant d'une
personne. Cette structure de clé est caractéristique d'une association
plusieurs-plusieurs entre les types *Immeuble* et *Personne*.

Finalement, nous savons que les associations plusieurs-un sont
représentées dans le schéma relationnel par les clés étrangères. Un
appartement est donc lié à un immeuble, une personne à un appartement.
Celles de la table *Propriétaire* ont déjà été prises en compte
puisqu'elles font partie de la représentation des associations
plusieurs-plusieurs. On obtient donc le schéma de la Fig.35.


<img src="schema-immeubles.png" alt="figure 35" width="500px">

> *Fig. 35.* Le schéma E/A des immeubles après rétro-conception

### Avec entités faibles


On pourrait, à partir de là, se poser des questions sur les choix de
conception. Une possibilité intéressante en l'occurrence est
d'envisager de modéliser les appartements par entité faible. Un
appartement est en effet une entité qui est très fortement liée à son
immeuble, et on peut très bien envisager un appartement comme un
*composant* d'un immeuble, en l'identifiant relativement à cet
immeuble. Le schéma E/A devient alors celui de la Fig.36.


<img src="schema-immeubles-avec-faible.png" alt="figure 36" width="500px">

> *Fig. 36.*Le schéma E/A des immeubles avec entité faible

L'impact sur le schéma relationnel est limité, mais quand même
significatif. La clé de la table *Appart* devient une paire
*(idImmeuble, no)*, et les clés étrangères changent également. On
obtient le schéma suivant.

 -   Immeuble (**id**, nom, adresse)
 -   Appart (**idImmeuble, no**, surface , niveau)
 -   Personne (**id**, prénom, nom , profession , *idImmeuble, no*)
 -   Propriété (**idPersonne, idImmeuble, no**, quotePart)

Il est important de noter que ce changement amènerait à modifier
également les requêtes SQL des applications existantes. *Tout changement
affectant une clé a un impact sur l'ensemble du système
d'information*. D'où l'intérêt (et même l'impératif) de bien
réfléchir avant de valider une conception.

### Avec réification

On pourrait aussi réifier l'association plusieurs-plusieurs pour une
faire un type d'entité *Propriétaire*. Le schéma entité-association est
donné par la `ea-immeubles-reifie`{.interpreted-text role="numref"}.


<img src="schema-immeubles-reifie.png" alt="figure 37" width="500px">

> *Fig. 37.* Le schéma E/A des immeubles avec réification

L'impact principal est le changement de la clé de la table
*Propriétaire* qui devient un identifiant propre. Voici le schéma
relationnel.

 -   Immeuble (**id**, nom, adresse)
 -   Appart (**idImmeuble, no** , surface , niveau)
 -   Personne (**id**, prénom , nom , profession , *idImmeuble, no*)
 -   Propriété (**id**, *idPersonne* , *idImmeuble, no*, quotePart)

Une conséquence importante est que la contrainte d'unicité sur le
triplet *(idImmeuble, no, idPersonne)* disparaît. On pourrait donc avoir
plusieurs nuplets pour la même personne avec le même immeuble. Dans ce
cas précis, cela ne semble pas souhaitable et on peut alors déclarer que
ce triplet *(idImmeuble, no, idPersonne)* est une clé candidate et lui
associer une contrainte d'unicité (nous verrons ultérieurement
comment). On peut aussi en conclure que la réification n'apporte rien
en l'occurrence et conserver la modélisation initiale.

Voici un petit échantillon des choix à effectuer pour concevoir une base
de données viable à long terme !


