L'ensemble des ressources de ce Module 4 Bases de données sont issues du [**cours de Philippe Rigaux**](http://sql.bdpedia.fr/).
Les contenus sont mis à disposition selon les termes de la licence Creative Commons Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions 4.0 International. 

# Sommaire B1-M4

## 1.4 Présentation du module : Bases de données

## 1.4.1 Introduction aux bases de données 

## 1.4.2 Le modèle relationnel

## 1.4.3 SQL, langage déclaratif

## 1.4.4 SQL, langage algébrique

## 1.4.5 SQL, récapitulatif

## 1.4.6 Conception d’une base de données

## 1.4.7 Schémas relationnel



