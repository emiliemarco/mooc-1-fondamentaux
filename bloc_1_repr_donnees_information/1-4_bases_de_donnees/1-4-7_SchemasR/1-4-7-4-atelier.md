## Atelier : étude du cas "Zoo", suite et fin


Allons-y pour la création de la base (normalisée) de notre zoo.

-   Donner les commandes de création des tables, avec clés primaire et
     clés étrangères.

 -   Ajouter la contrainte suivante : deux animaux de la même espèce ne
     doivent pas avoir le même nom.

 -   Reprendre le contenu de la table non-normalisée (ci-dessous), et
     donner les commandes d\'insertion de ces données dans la base
     normalisée.

 -   Exprimer les requêtes suivantes :

      -   Quels sont les ours du zoo ?
      -   Quels animaux s\'appellent Jojo ?
      -   Quels animaux viennent de la planète Kashyyyk (quand ils ne
          sont pas prisonniers dans le zoo\...) ?
      -   De quels animaux s\'occupe le gardien Jules ?
      -   Sur quel(s) emplacement(s) y-a-il des animaux de classes
          différentes (donner aussi le nom du gardien) ?
      -   Somme des salaires des gardiens.
      -   Quels gardiens surveillent plus d\'un emplacement ?
      

 -   Et pour finir, donner la définition de la vue qui recrée, à partir
     de la base normalisée, le contenu de la table avant décomposition
     (ci-dessous).


 ---------------------------------------------------------------------------------------------------------------
  codeAnimal  |  nom    |      espèce   |     gardien  |  salaire   | classe   |    origine    |     codeEmplacement  |  surface
  --- | --- | --- | --- | --- | --- | --- | --- | ---  
  10  |          Zoé    |      Girafe  |     Marcel |  20 000    | Mammifère   |  Afrique   |    A   |      120
  20  |       Chewbacca |  Wookiee     |    Marcel  |  20 000   |             |    Kashyyyk    |  C    |  200 
  30  |         Jojo    |    Mérou     |   Jules    |  18 000    | Poisson     |  Méditerranée | B      |            50
  20  |         Irma    |  Perroquet   | Marcel     |  20 000    | Oiseau      |  Asie         |  A     |             120
  40  |        Goupil   |     Renard   |     Jules  |    18 000  | Insecte     |  Europe       |  B     |           50



À vous de jouer !
