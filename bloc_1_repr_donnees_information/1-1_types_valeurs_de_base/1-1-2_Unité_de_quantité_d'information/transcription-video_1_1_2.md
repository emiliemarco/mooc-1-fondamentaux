# MOOC NSI - fondamentaux. 
## Transcription de la vidéo

## 1.1.2 : Unités de quantité d'information

Quand nous achetons un ordinateur ou un smartphone, ses caractéristiques techniques contiennent, entre autres, la quantité d'information que l'on peut stocker dans sa mémoire. Celle-ci est normalement exprimée en giga-octets, qu'on écrit Go. 

Le giga-octet est donc une unité qui permet de parler de la quantité d'information. C'est le sujet de cette vidéo: nous allons voir comment mesurer l'information quand elle est exprimée en binaire.


Chaque chiffre (0 ou 1) d'une représentation binaire est appelé un bit. Ce nom vient de l'anglais binary digit, qui signifie chiffre binaire

Quand on a un nombre de 8 bits, on parle d'octet en français, ou de byte en anglais. Voici un exemple où l'on a bien huit chiffres

Les symboles associés à ces unités sont
le b pour le bit
le o pour l'octet
le B majuscule pour le byte


Naturellement, les quantités d'information qu'on manipule dans la vie de tous les jours sont beaucoup plus importantes. On doit donc pouvoir parler de milliers d'octets, de millions d'octets, etc.

Pour ce faire, on trouve en informatique deux types de préfixes. 

On peut utiliser les préfixes habituels kilo-, méga-, etc du système SI. Ainsi, un kilooctet s'écrit "k o" et vaut mille octets. Un mégaoctet, s'écrit "M majuscule o" et vaut un million d'octets, etc

On peut aussi utiliser des préfixes propres à l'informatique, qui sont basés sur des puissances de 2. On parle alors de kibioctet pour 2 exposant 10 octets, ce qui se note "k i o". Un mébioctet vaut 2 exposant 20 octets, et se note "M majuscule i o". Et ainsi de suite.

En pratique, ces deux familles d'unités sont suffisamment proches qu'on se permet souvent de les confondre. Néanmoins, des procès retentissants ont eu lieu au début des années 2000 autour de cette confusion, car des consommateurs pensaient avoir acheté un disque dur d'une capacité équivalente à 80 Gibioctet, alors qu'ils ne contenaient en réalité que 80 Gigaoctets... 

