Nous allons maintenant rappeler les techniques permettant de
représenter des nombres dans différentes \emph{bases}, car ces
techniques serviront à expliquer les différents encodages binaires des
nombres entiers et rationnels.

La représentation usuelle des nombres est une représentation
\emph{décimale} et \emph{positionnelle}. Cela signifie:
\begin{enumerate}
\item que les nombres
sont représentés par une séquence de \emph{chiffres} de $0$ à $9$
inclus. Il y a bien \emph{dix} chiffres différents utilisés, d'où le
terme \emph{décimal}; et
\item que la \emph{position} de chaque chiffre dans la séquence
permet d'associer ce chiffre à une \emph{puissance de 10}.
\end{enumerate}
Dans cette représentation usuelle, 10 est appelé la \emph{base}. On
parle aussi de représentation en base 10.

\exemple{
  Par exemple, la représentation:

  \[
    1234,56
  \]

  signifie que le nombre est composé de:
  \begin{itemize}
  \item $4$ unités, ou $4\times 10^0$;
  \item $3$ dizaines, ou $3\times 10^1$;
  \item $2$ centaines, ou $2\times 10^2$;
  \item $1$ millier, ou $1\times 10^3$;
  \item $5$ dixièmes, ou $5\times 10^{-1}$; et
  \item $6$ centièmes, ou $6\times 10^{-2}$.
  \end{itemize}
  On voit donc que les puissances de $10$ associées aux différentes
  positions (aux différents chiffres) vont décroissantes quand on
  lit le nombre de gauche à droite, et que la puissance $10^0$
  correspond au chiffre qui se trouve juste à gauche de la virgule.
}

On note également que la base donne le nombre de chiffres que l'on
peut utiliser pour représenter les nombres. En base 10, on peut
utiliser 10 chiffres différents, à savoir les chiffres de $0$ à $9$
inclus.

Ces concepts se généralisent dans n'importe quelle base. Fixons à
partir de maintenant une base $b\geq 2$. Dans cette base, le nombre:

\[
  d_n\cdots d_0\ ,\ d_{-1}\cdots d_{-k}
\]

représente la valeur:

\begin{eqnarray*}
  N& =&d_n\times b^n + d_{n-1}\times b^{n-1}+\cdots+d_0\times
        b^0+d_{-1}\times b^{-1}+\cdots d_{-k}\times b^{-k}\\
     &=&\sum_{i=-k}^{n}d_i\times b^i &\; \;\;\;[1]
\end{eqnarray*}

où tous les $d_i$ sont des chiffres dans $\{0,\ldots b-1\}$.

\exemple{
  Si on fixe $b=2$, le nombre $1001,{}101$ représente:
  
  \begin{eqnarray*}
    &&1\times 2^3 + 0\times 2^2 + 0\times 2^1+ 1\times 2^0 + 1\times
    2^{-1}+0\times 2^{-2}+1\times 2^{-3}\\
    &=&8 + 0 + 0 + 1 + \frac{1}{2} + 0 + \frac{1}{8}\\
    &=&9,625.
  \end{eqnarray*}
  
  La dernière valeur est bien entendu donnée en base $10$.

  Symétriquement, la valeur $-4,5$ (en base $10$) est représentée  par:

  \[
    -100,1
  \]

  en base $2$. Notons au passage que cette dernière représentation
  n'est pas \textit{stricto sensu} une représentation \emph{binaire},
  étant donné que les symboles $,$ (virgule) et $-$ (moins unaire)
  sont utilisés, en plus du $0$ et du $1$. Nous verrons plus tard
  comment une telle valeur peut être représentée en utilisant des $0$
  et des~$1$ uniquement.  }

Afin d'éviter toute confusion dans la suite, nous noterons, quand
c'est nécessaire, la base en indice des nombres. Par exemple, $101_2$
signifie que le nombre $101$ doit être interprété comme étant en base
$2$ (c'est-à-dire $5_{10}$). Notons que dans certains langages de
programmation, comme le C \cite{Cbook}, on utilise d'autres
conventions pour expliciter certaines bases: les nombres en base $16$
son préfixés par \texttt{0x}, et les nombres en base $2$ par
\texttt{0b}. Enfin, notons qu'en base $2$, nous grouperons souvent les
bits par paquets de $4$, en introduisant un petit espace, et ce,
uniquement dans un souci de lisibilité. Nous écrivons ainsi
$1011\ 1001$ au lieu de $10111001$.

En informatique, les bases usuelles sont la base $2$, la base $8$ (on
parle d'octal) et la base $16$ (on parle d'hexadécimal). Pour la base
$16$, on est confronté au problème suivant: les chiffres utilisés
devraient être $0$, $1$,\ldots $10$, $11$, $12$, $13$, $14$ et
$15$. Mais les ``chiffres'' de $10$ à $15$ demandent deux symboles
pour être représentés, ce qui risque de porter à confusion. Par
exemple, doit-on interpréter $10_{16}$ comme le chiffre $10$ (et alors
$10_{16}=10_{10}$) ou comme les chiffres $1$ et $0$ (et donc
$10_{16}=1\times 16+0\times 1=16_{10}$) ? Pour éviter ce problème, on
remplace les ``chiffres'' de $10$ à $15$ par les lettres de
\texttt{a} à \texttt{f}, selon la correspondance suivante:

\begin{center}
  \begin{tabular}{c|cccccc}
    \toprule
    Lettre:&\texttt{a}& 
    \texttt{b}&
    \texttt{c}& 
    \texttt{d}& 
    \texttt{e}& 
    \texttt{f}\\
    \midrule
    Valeur:& 10& 11& 12 & 13 & 14 & 15 \\
    \bottomrule
  \end{tabular}
\end{center}

\exemple{
  En base 16:
  
  \begin{eqnarray*}
    \mathtt{a}5\mathtt{f},\mathtt{b} &=&10\times 16^2 + 5\times 16^1 + 15\times 16^0 + 11\times 16^{-1}\\
    &=&2655,6875_{10}.
  \end{eqnarray*}
}

Sur base de ces définitions, nous pouvons déjà faire quelques
remarques utiles.

\paragraph{Chiffres de poids fort, de poids faible}
Dans la représentation positionnelle usuelle, le chiffre le plus à
gauche est associé à une puissance plus élevée de la base. On parle
donc de chiffre de ``poids fort''{}; le chiffre étant associé à la
puissance la plus faible est appel  chiffre de ``poids faible''. En
particulier, en binaire, on parle de \emph{bit de poids fort} et
\emph{bit de poids faible}.

Comme nous l'avons dit, le bit de poids fort est, par convention à
gauche dans la représentation (positionnelle) usuelle. Mais quand on
s'intéresse à des valeurs qui ne sont plus écrites sur papier, mais
bien stockées ou manipulées par des circuits électroniques (où les
notions de ``gauche'' et ``droite'' n'ont pas forcément de sens),
il est parfois utile de préciser explicitement quel est le bit de
poids fort ou le bit de poids faible.

\paragraph{Ajouts de zéros}
Dans toutes les bases, et à condition d'utiliser la représentation
positionnelle usuelle (avec le chiffre de poids fort à gauche), on peut
toujours ajouter des $0$ à gauche de la partie entière ou à droite de
la partie décimale, sans modifier la valeur du nombre représentée. Par
exemple, $13,5_{10}=0013,5000_{10}$.  Par contre, on ne peut pas
ajouter de zéros à d'autres endroits sans modifier la valeur du
nombre. Par exemple, $101_2\neq 101000_2$ (voir aussi la remarque
suivante).

\paragraph{Multiplier ou diviser par la base} Dans toutes les bases,
on peut aisément multiplier ou diviser par la base en déplaçant la
position de la virgule:

\mynote{ Dans toute base $b$, déplacer la virgule de $k$ position vers
  la \textbf{droite} ( ou ajouter $k$ zéros à droite dans le cas d'un
  nombre entier) revient à \textbf{multiplier} par $b^k$. Déplacer la
  virgule de $k$ positions vers la \textbf{gauche} revient à
  \textbf{diviser} par $b^k$. Si on souhaite faire une division
  entière par $b^k$, le \textbf{reste} est constitué des $k$ chiffres de poids
  faible, et on obtient le \textbf{quotient} en retranchant ces $k$
  chiffres de poids faible. }

Nous utiliserons les opérateurs $\div$ et $\bmod$ pour noter
respectivement  la division entière et le reste de la division. Voici
quatre exemples qui illustrent ces remarques:

\exemple{
  
  \begin{eqnarray*}
    101,111_2\times 4_{10} &=&  101,111_2\times 2^2\\
    &=&10111,1_2\\
    101,111_2 / 2_{10} &=& 10,1111_2\\
     1011\ 1011_2 \div 100_2 &=& 1011\ 1011_2\div 2^2\\
                            &=&10\ 1110\\
    1011\ 1011_2 \bmod 100_2 &=& 11_2.
  \end{eqnarray*}
}

% \exemple{
%   Voici des exemples de multiplication et de division par une
%   puissance de la base, en base $2$:
%   \begin{eqnarray*}
%     101,111_2\times 4_{10} &=&10111,1_2\\
%     101,111_2 / 2_{10} &=& 10,1111_2.\\
%   \end{eqnarray*}
% }

% \exemple{ Voici des exemples de division entière et de reste par une
%   puissance de la base, en base $2$:
%   \begin{eqnarray*}
%     1011\ 1011_2 \div 100_2 &=& 1011\ 1011_2\div 2^2\\
%                             &=&10\ 1110\\
%     1011\ 1011_2 \bmod 100_2 &=& 11_2.
%   \end{eqnarray*}
% }

\paragraph{Combien de nombres représentables sur $n$ chiffres ?}
Enfin, l'observation suivante aura toute son importance dans la
suite. Fixons une base $b$ et considérons des nombres entiers positifs
sur $n$ chiffres. Nous avons $b$ choix pour chacun de ces $n$
chiffres, soit un total de $b^n$ nombres différents que l'on peut
représenter sur $n$ chiffres. Ces nombres sont tous les nombres de:

\[
  \underbrace{0\cdots 0}_{n\text{ chiffres}}
  \]
  
 à:
  
\[
  \underbrace{(b-1)\cdots (b-1)}_{n\text{ chiffres}} = b^n-1.
\]

\mynote{ En base $b$, il y a $b^n$ nombres (entiers positifs)
  représentables sur $n$ chiffres: les nombres de $0$ à $b^n-1$.  }

Notons que quand nous souhaiterons représenter des nombres négatifs de
manière purement binaire (donc, sans utiliser le symbole -), nous
aurons besoin d'utiliser certaines de ces représentations pour
``encoder'' des nombres négatifs.

\exemple{ Il y a donc $2^n$ nombres binaires sur $n$ bits. Par
  exemple, il y a $2^{32}=4\ 294\ 967\ 296_{10}$ (soit à peu près $4$
  milliards de) nombres binaires différents sur $32$ bits.  }

