# MOOC NSI - fondamentaux. 
## Transcription de la vidéo

## 1.1.1 : Représentation binaire de l'information



Un ordinateur est une machine qui traite de l'information. Pour comprendre comment, nous devons d'abord expliquer comment l'information est représentée.

En effet, il faut bien faire la différence entre l'information et sa représentation. Cela peut sembler étrange comme question... Par exemple, si on considère la valeur 17, tout le monde pensera à représenter cette valeur par les chiffres 1 et 7.

Mais on peut imaginer plein d'autres possibilités, par exemple, les chiffres romains, les marques de dénombrement, ou encore en binaire. Et on pourrait encore en imaginer d'autres...

La représentation binaire est justement celle que nous allons utiliser. 

Mais qu'entend-on par "binaire" ? Cela signifie tout simplement que TOUTE l'information est représentée à l'aide de deux symboles uniquement. 

Ceux-ci sont en général appelés "0" et "1", mais cela pourrait aussi être "faux" et "vrai"...

Pourquoi avoir adopté le binaire comme représentation universelle en informatique ? Pour des raisons technologiques: on peut facilement représenter deux valeurs différentes avec des voltages différents. Par exemple entre 0 et 1,4 volts, on aura un "0", et entre 2,4 et 5V on aura un "1". Ces voltages peuvent alors être manipulés par les composants de base de l'ordinateur que sont les transistors.


Dans la suite de ce bloc, nous allons voir comment on peut représenter des nombres, du texte ou des images de façon binaire...



