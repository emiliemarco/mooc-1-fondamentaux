1
00:00:00,744 --> 00:00:01,940
Dans la vidéo précédente,

2
00:00:02,040 --> 00:00:03,415
nous avons vu comment représenter

3
00:00:03,515 --> 00:00:04,854
les nombres entiers et positifs

4
00:00:04,954 --> 00:00:06,107
dans une base arbitraire.

5
00:00:06,592 --> 00:00:08,519
Comment faire si le nombre n'est pas entier,

6
00:00:08,619 --> 00:00:10,863
c'est-à-dire s'il a des chiffres après la virgule ?

7
00:00:11,493 --> 00:00:14,770
Considérons le nombre 6,625

8
00:00:14,870 --> 00:00:16,517
que nous voulons exprimer en base 2.

9
00:00:16,775 --> 00:00:19,504
Cela revient à le décomposer en une somme de puissances de 2

10
00:00:19,604 --> 00:00:21,437
et nous savons déjà que la partie entière, 6,

11
00:00:21,537 --> 00:00:23,933
s'exprime 110 en base 2.

12
00:00:24,489 --> 00:00:26,545
Pour traiter le 0,625,

13
00:00:26,645 --> 00:00:28,847
nous allons le multiplier par 2 de manière répétée.

14
00:00:29,553 --> 00:00:33,002
0,625 fois 2 donne 1,25

15
00:00:33,271 --> 00:00:34,967
Ce résultat est plus grand que 1,

16
00:00:35,067 --> 00:00:37,286
ce qui nous indique que 0,625

17
00:00:37,386 --> 00:00:38,269
est plus grand qu'un demi.

18
00:00:38,369 --> 00:00:39,520
Nous devons donc mettre à 1

19
00:00:39,620 --> 00:00:41,338
le bit correspondant à un demi.

20
00:00:41,525 --> 00:00:44,005
Il nous reste alors à traiter la partie fractionnaire

21
00:00:44,105 --> 00:00:45,461
c'est-à-dire 0,25.

22
00:00:45,950 --> 00:00:47,983
À nouveau, nous la multiplions par 2,

23
00:00:48,083 --> 00:00:49,332
ce qui donne 0,5.

24
00:00:49,432 --> 00:00:51,559
Le résultat est plus petit que 1,

25
00:00:51,659 --> 00:00:53,878
le bit suivant doit donc être mis à 0.

26
00:00:54,363 --> 00:00:56,942
On multiplie à nouveau la partie fractionnaire par 2

27
00:00:57,042 --> 00:00:58,704
et on obtient cette fois-ci 1.

28
00:00:58,804 --> 00:01:01,792
Le dernier bit est donc un 1 également.

29
00:01:02,354 --> 00:01:04,404
Le processus s'arrête car la partie fractionnaire

30
00:01:04,504 --> 00:01:05,751
est maintenant nulle.

31
00:01:06,794 --> 00:01:08,254
Dans cet exemple, 

32
00:01:08,354 --> 00:01:10,835
la procédure de conversion en base 2 s'est terminée.

33
00:01:10,935 --> 00:01:12,314
Mais ça ne sera pas toujours le cas.

34
00:01:12,475 --> 00:01:13,781
Certains nombres

35
00:01:13,881 --> 00:01:16,175
ont une représentation finie dans certaines bases

36
00:01:16,275 --> 00:01:17,378
mais pas dans d'autres.

37
00:01:18,101 --> 00:01:19,298
Par exemple,

38
00:01:19,401 --> 00:01:22,615
le nombre 1/3 s'écrit 0,1 en base 3

39
00:01:22,715 --> 00:01:25,929
mais a une représentation infinie en base 10.

40
00:01:26,842 --> 00:01:28,223
Quand on a une partie fractionnaire

41
00:01:28,323 --> 00:01:29,942
qui contient une même séquence de chiffres

42
00:01:30,042 --> 00:01:31,525
qui se répète infiniment souvent,

43
00:01:31,625 --> 00:01:35,073
on parle de représentation infinie périodique.

44
00:01:35,173 --> 00:01:37,290
On écrit une seule fois la période

45
00:01:37,390 --> 00:01:38,883
et on la souligne.

46
00:01:39,618 --> 00:01:41,670
Cela peut aussi avoir lieu en base 2.

47
00:01:42,731 --> 00:01:45,704
Considérons le nombre 0,54 en base 10.

48
00:01:46,363 --> 00:01:48,376
Appliquons la procédure vue précédemment.

49
00:01:49,345 --> 00:01:50,596
Après 4 étapes,

50
00:01:50,709 --> 00:01:53,365
nous constatons que la même partie fractionnaire

51
00:01:53,465 --> 00:01:55,156
0,8 se répète.

52
00:01:55,256 --> 00:01:57,555
Cette fois-ci, la procédure ne va donc pas terminer

53
00:01:57,655 --> 00:02:00,007
mais va produire la même séquence de chiffres

54
00:02:00,107 --> 00:02:02,689
 1100 répétée infiniment suivant.

55
00:02:02,850 --> 00:02:05,049
0,54 s'exprime donc

56
00:02:05,149 --> 00:02:06,846
de façon infinie périodique

57
00:02:06,946 --> 00:02:07,761
en base 2.

58
00:02:08,239 --> 00:02:10,819
Rappelons aussi qu'il existe des nombres irrationnels

59
00:02:10,919 --> 00:02:13,293
comme pi, racine de 2 ou la constante e.

60
00:02:13,393 --> 00:02:14,343
Ceux-ci n'admettent

61
00:02:14,443 --> 00:02:15,955
ni une représentation finie

62
00:02:16,055 --> 00:02:18,668
ni même une représentation infinie périodique

63
00:02:18,768 --> 00:02:20,535
et ce, dans aucune base entière.

64
00:02:20,948 --> 00:02:23,568
Enfin, notons qu'une représentation en base 2

65
00:02:23,668 --> 00:02:25,728
d'un nombre avec une partie fractionnaire

66
00:02:25,828 --> 00:02:28,055
n'est pas une représentation binaire.

67
00:02:28,155 --> 00:02:28,960
En effet,

68
00:02:29,060 --> 00:02:30,512
nous avons besoin de trois symboles,

69
00:02:30,612 --> 00:02:33,329
le 0, le 1 mais aussi la virgule.

70
00:02:33,482 --> 00:02:34,758
Nous verrons plus tard

71
00:02:34,858 --> 00:02:36,261
comment éviter la virgule.

72
00:02:36,361 --> 00:02:37,572
Cela nous permettra

73
00:02:37,672 --> 00:02:39,335
de représenter les nombres fractionnaires

74
00:02:39,435 --> 00:02:40,713
dans une mémoire d'ordinateur

75
00:02:40,813 --> 00:02:42,402
à condition qu'ils ne nécessitent

76
00:02:42,502 --> 00:02:44,702
qu'un nombre fini de 0 et de 1.

