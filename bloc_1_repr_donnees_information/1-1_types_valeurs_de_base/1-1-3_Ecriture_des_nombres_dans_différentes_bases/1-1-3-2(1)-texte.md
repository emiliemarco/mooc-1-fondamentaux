Comment peut-on, étant donné un nombre $N$ donné dans une certaine base,
trouver sa représentation dans une autre base $b$ ? Répondre à cette
question revient à calculer les valeurs $d_i$ de l’équation $[1]$ dans
la base $b$.

#### Cas des nombres entiers

Nous allons commencer par supposer que $N$ est un nombre entier positif.
Observons d’abord que si $N<b$, on exprime le nombre à l’aide du chiffre
correspondant dans la nouvelle base[^1]. Autrement, divisons $N$ par $b$
(il s’agit de la division entière), ce qui nous donne un quotient
$q_0=N\div b$ et un reste $r_0=N\mod b$. La relation existant entre ces
valeurs est:

$$\begin{aligned}
  N&= q_0\times b +r_0.&[2]\end{aligned}$$

Comme nous avons supposé que $N\geq b$, nous savons que $q_0\neq0$.
Recommençons l’opération en divisant $q_0$ par $b$; nous obtenons à
nouveau un quotient que nous appelons $q_1$ et un reste que nous
appelons $r_1$. Nous avons donc la relation:

$$\begin{aligned}
  q_0&= q_1\times b+r_1,\end{aligned}$$

En combinant cette équation avec $[2]$, nous obtenons:

$$\begin{aligned}
  N&= (q_1\times b+r_1)\times b+r_0\\
  &= q_1\times b^2+r_1\times b+r_0.
  \end{aligned}$$

Si $q_1>0$, on recommence en divisant $q_1$ par $b$, pour obtenir un
nouveau quotient $q_2$ et un nouveau reste $r_2$ tels que:

$$\begin{aligned}
  N&= (q_2\times b+r_2)\times b^2+r_1\times b+r_0\\
  &= (q_2\times b^3)+r_2\times b^2+r_1\times b+r_0.\end{aligned}$$

Si nous continuons ce développement en réalisant $k+1$ divisions, nous
obtenons une suite de restes $r_0$, $r_1$,…$r_k$ tels que:

$$\begin{aligned}
  N  &= (q_k\times b^{k+1})+r_k\times b^k+\cdots+r_2\times b^2+r_1\times b+r_0.\\\end{aligned}$$

Supposons que nous avons continué ce développement jusqu’à ce que
$q_k=0$ (ce qui finira toujours par arriver étant donné qu’on est parti
d’un $N$ fixé). On a alors:

$$\begin{aligned}
  N  &=(q_k\times b^{k+1})+r_k\times b^k+\cdots+r_2\times
       b^2+r_1\times b+r_0\\
  &=(q_k\times b^{k+1})+r_k\times b^k+\cdots+r_2\times b^2+r_1\times
    b^1+r_0\times b^0\\
  &=(q_k\times b^{k+1})+\sum_{i=0}^{k}r_i\times b^i \\
  &=(0\times b^{k+1})+\sum_{i=0}^{k}r_i\times b^i \\
    &=\sum_{i=0}^{k}r_i\times b^i.&[3]\end{aligned}$$

On peut maintenant comparer cette dernière équation $[3]$ à
l’équation $[1]$, et constater que les restes $r_i$ que nous avons
calculés en effectuant des divisions successives par $b$ correspondent
aux $d_i$ que nous cherchons pour représenter le nombre $N$ en base $b$.
Attention tout de même que le premier reste calculé ($r_0$) est le
chiffre de poids faible ! Pour résumer:

**Note :**

Pour exprimer un nombre $N$ en base $b$, on divise $N$ par $b$ de
manière répétée. La séquence des restes calculés donne la représentation
de $N$ en base $b$ du chiffre de poids faible au chiffre de poids fort
(autrement dit: “à l’envers”).

$\square$

[^1]: Par exemple, $10_{10}$ est exprimé par $\mathtt{a}$ en base $16$.
