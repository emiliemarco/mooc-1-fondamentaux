# MOOC NSI - fondamentaux. 
# Transcription de la vidéo

## 1.1.3.3 : Manipuler les données en binaire

Dans cette vidéo, nous allons voir quels types d'opérations on peut appliquer à des nombres en binaire.

Tout d'abord, nous pouvons effectuer les opérations arithmétiques classiques en binaire. Faisons la somme de 111 et 110. Comme en base 10, on commence par la colonne de droite: 1 + 0 = 1. Passons à la colonne suivante: 1 + 1 = 2 qui s'écrit 10 en binaire. On recopie donc le bit de poids faible, 0, dans la solution et on reporte le bit de poids fort, 1. Pour la dernière colonne, 1 + 1 + 1 = 11 en binaire. On ajoute donc ces deux bits à la solution. 

La soustraction, la multiplication et la division écrites fonctionnent toujours en binaire.

Comme les symboles binaires 1 et 0 peuvent être interprétés comme vrai et faux, on peut aussi utiliser des opérateurs logiques. Les quatre opérateurs de base sont le "et" (qu'on note and en Python), le "ou inclusif" (or en Python), le "ou exclusif" (accent circonflexe en Python) et la négation (not en Python).

Pour définir ces opérateurs, on utilise une table de vérité. Voici celle de l'opérateur "et". Elle donne la valeur de "a et b" pour toute combinaison de valeurs binaires a et b. La définition du et correspond à son intuition: a et b vaut 1 uniquement quand a ET b valent 1 en même temps.

Voici les tables de vérité des trois autres opérateurs. 

On notera la différence entre les deux types de ou, et le fait que le not est un opérateur unaire.

On peut maintenant appliquer ces opérateurs à des nombres en binaire, bit à bit, c'est-à-dire colonne par colonne. Voici un exemple avec le et de deux nombres binaires. Le langage Python dispose d'opérateurs pour réaliser ces opérations.

Notons que l'opérateur "et" permet de réaliser un masque. Cela revient à sélectionner certains bits d'un nombre et à remplacer les autres par des 0. Les bits sélectionnés sont ceux où le masque est à "1".

Une autre opération très utile en binaire et dans les autres bases est le décalage. Le décalage à gauche revient à ajouter des zéros à droite du nombre, de manière à décaler les chiffres vers la gauche. 

Supposons qu'on veuille, en base 10, multiplier le nombre 13 par 1000. Cela revient à multiplier par 10^3, donc à décaler 13 de trois positions vers la gauche.

Cela fonctionne également en binaire: si on veut multiplier 1101 par 8, cela revient à un décalage de trois positions vers la gauche en binaire. On décale de trois positions car 8 = 2^3. De manière générale, ajouter un "0" à droite revient à multiplier par la base.

De façon symétrique, le décalage à droite revient à effacer des chiffres de poids faible pour tout décaler vers la droite. Cela revient à diviser par la base, et les chiffres effacés constituent le reste de la division

Par exemple 1101 0110 divisé par 8 donne 11010, avec un reste de 110.





