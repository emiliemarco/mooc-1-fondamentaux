# Notre exerciseur UpyLaB 

Les exercices de programmation de ce cours demandent l'utilisation d'un interpréteur Python 3 et d'un environnement de développement. L'outil  **Python Tutor** peut également être utile pour comprendre tous les mécanismes de Python.

Par ailleurs, nous utilisons de façon intégrée le **juge en ligne UpyLaB** (https://upylab2.ulb.ac.be) pour valider vos codes. Dans cette partie du cours, il ne vous sera pas utile de créer un comptre sur UpyLaB, même s'il est ouvert à tous (en mode élève ou enseignant).

> `UpyLaB` est notre plateforme d’apprentissage en ligne. Elle propose des
>  exercices de codage et demande aux étudiants de produire le code
>  Python correspondant ; ensuite UpyLaB, qui est exécuté sur un serveur
>  extérieur à la plateforme FUN, teste si la solution proposée est
>  correcte. Nous avons intégré l’utilisation d’UpyLaB au cours en ligne,
>  comme le montre le premier exercice UpyLaB proposé ci-dessous.

#### MODE D'EMPLOI

Le premier exercice UpyLaB ci-après vous permet de tester l'utilisation
d'UpyLaB dans le cours.

Le principe consiste à lire l'énoncé de l'exercice concerné puis à
résoudre le problème dans votre environnement de développement (Thonny par exemple).
Une fois que vous vous êtes assuré que votre script répond aux exigences de l'énoncé, il faut le **recopier** dans la partie de fenêtre UpyLaB servant à cet effet (mini-fenêtre vide au départ, sinon qui contient votre dernière proposition de solution) pour le soumettre à évaluation en cliquant sur le bouton "Valider".


Quand l'utilisateur a cliqué sur le bouton "Valider", UpyLaB va demander à l'interpréteur Python 3 d'exécuter le code fourni.  Il va ensuite comparer le résultat obtenu avec
ce qui est attendu et le valider si les deux coïncident.

UpyLaB effectue un ou plusieurs tests selon les exercices, et ensuite, en fonction des résultats de ces tests,
vous indique si le code soumis lui semble correct ou non par rapport à
ce qui vous est demandé.

<!-- Comme pour l’installation de Python 3, en cas de besoin, nous vous
proposons d’utiliser la FAQ et si vos soucis ne sont pas résolus, le
forum UpyLaB du présent module afin de vous mettre en contact avec les
autres apprenants et l'animateur des forum.

En particulier, si vous rencontrez des soucis pour réaliser l'exercice
1.1 d'UpyLaB, décrivez le plus clairement possible le problème |nbsp| : notre
équipe ou d’autres apprenants plus expérimentés pourront probablement
vous aider.-->



## Exercice UpyLaB 1.1 n° 1 

Les seuls buts de cet exercice sont de vérifier que vous avez accès à notre exerciseur UpyLaB et de vous donner un premier contact avec cet outil.

Écrire un programme qui affiche "Bonjour UpyLaB !" grâce à l'instruction :

```python
  print("Bonjour UpyLaB !")
```

Veillez à ne pas ajouter d’espace au début de la ligne et à parfaitement respecter les espaces, minuscules et les majuscules (ici pourquoi ne pas simplement faire une copier/coller de l'instruction dans la fenêtre UpyLaB) 

Pour tous les exercices UpyLaB, vous devez écrire **le code Python qui solutionne le problème (c'est-à-dire votre programme Python)** dans la sous-fenêtre intitulée "Votre solution".

### Exemple

Lorsqu'il est exécuté, le programme affiche :

```
 Bonjour UpyLaB !
```

Bien sûr, libre à vous d'être curieux et de voir ce que répond UpyLaB quand vous donnez une autre réponse, par exemple :

  ```python
     print("Bonsoir UpyLaB !")
  ```
  
ou en ajoutant une ou plusieurs espaces devant l'instruction.
