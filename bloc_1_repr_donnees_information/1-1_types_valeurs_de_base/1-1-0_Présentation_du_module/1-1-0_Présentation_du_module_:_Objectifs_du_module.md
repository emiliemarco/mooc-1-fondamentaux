## Objectifs du module

[![Vidéo 0 B1-M1-S0 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B1-M1-S0.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B1-M1-S0.mp4)

<h4>Pr&eacute;requis</h4>
<p>Aucun pr&eacute;requis n'est n&eacute;cessaire pour suivre ce module</p>

<div class="in-brief">
    <div class="brief">     
        <div class="brief-img">
          <i class="ill-icon ill-icon-list"></i>
        </div>
   <div class="brief-text">
     <b>Sommaire : </b>
   </div> 
  </div>
</div>

<p>Ce Module s'appuie sur le support de cours écrit portant le même titre : <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/blob/master/__supports-de-cours/B1-M1/texte-mooc-1-1-1_pdf_initial/main.pdf" target=_"blank"><strong>Représentation des données : types et valeurs de base</strong></a></p>

<div style="margin-left:68px; margin-top:-20px; margin-right:90px;">
<ul>
  <li>1. Représentation binaire de l'information</li>
<li>2. Unité de quantité d'information</li>
<li>3. Écriture des nombres dans différentes bases</li>
<ul>
<li>3-1. Introduction : la notion de base</li>
 <li>3-2. Changement de base</li>
 <li>3-3. Manipuler les données en binaire</li>
 <li>3-4. Nombres signés</li>
  <li>3-5. Nombres avec partie fractionnaire</li>
  </ul>
<li>4. Représentation du texte</li>
<li>5. Représentation d'images, de sons, de vidéos</li>
<li>6. Exercices</li>
</ul>
</div>
<div class="in-brief">
<div class="brief">
        <div class="brief-img">
          <i class="ill-icon ill-icon-clock"></i>
        </div>
        <div class="brief-text">
          <p>
            <b>Temps d'investissement : </b>4 à 6 heures<br/>
       </div>
      </div>
  </div>
<div style="margin-left:68px; margin-top:-30px;">
            <i>Ce temps est donné à titre indicatif. Il peut varier en fonction des participants.</i>



## Enseignant
**Gilles Geeraerts, Université Libre de Bruxelles**

Gilles Geeraerts est docteur en sciences informatiques de l’Université libre de Bruxelles. Il y est chargé de cours et y enseigne les principes de base du fonctionnnement des ordinateurs, ainsi que la théorie des langages et de la compilation.

[Site web de l'enseignant](http://di.ulb.ac.be/verif/ggeeraer/#Teaching)
