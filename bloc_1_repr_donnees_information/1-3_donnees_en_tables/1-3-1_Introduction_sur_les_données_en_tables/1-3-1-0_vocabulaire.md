## Introduction

La modélisation informatique d'un problème concret (gérer les milliers de patients d'un hôpital, ou les articles, clients, commandes d'un site de vente) nécessite le plus souvent de manipuler différents _acteurs_ (des objets, des personnes) et des relations entre ces acteurs, en très grand nombre. On parle de **donnée** plutôt que d'_acteur_ et les relations sont aussi de la donnée. On se retrouve donc avec des données, nombreuses, très nombreuses, de plus en plus nombreuses : au moment où ce texte est écrit, le volume de données publié sur internet par seconde est 29000 Go.

Une organisation hiérarchique des ces données n'est plus possible. Dès les années 60, des travaux ont été conduits pour trouver une façon de les gérer : les stocker, les interroger, les modifier. Les SGBD (Systèmes des Gestion de Bases de Données) sont la réponse que nous étudierons dans le prochain chapitre.

Dans celui-ci, nous allons nous intéresser à des données que l'on peut représenter sous la forme de lignes contenant plusieurs colonnes : 

- chaque ligne représente une donnée : on parle d'**enregistrement** 
- et chaque colonne correspond à une information à propos de cette donnée : ce sont les **attributs**. Ces colonnes sont parfois nommées, on parle alors de **descripteurs**. 

Les enregistrements d'une même table partagent donc les mêmes descripteurs, dans le même ordre. Pour désigner l'ensemble des valeurs d'une colonne on parle de **champs**. 

Les attributs d'un enregistrement sont d'un certain type (chaîne de caractères, nombre,...) et parfois d'un champs de valeurs particulier (par exemple une adresse mail appartient à un sous-ensemble strict des chaînes de caractères).

Ces lignes et ces colonnes forment tout naturellement un tableau, on parle de **table**. Ces tables permettent la manipulation des données. Un simple tableur peut suffir à effectuer certaines manipulations via des fonctions dédiées. Ici, nous allons voir comment réaliser ces traitements à l'aide du langage Python. 

