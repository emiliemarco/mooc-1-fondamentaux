1
00:00:05,068 --> 00:00:07,344
Nous continuons à manipuler les tables

2
00:00:07,444 --> 00:00:09,039
et nous allons voir maintenant

3
00:00:09,450 --> 00:00:11,326
comment travailler avec plusieurs tables.

4
00:00:11,961 --> 00:00:15,147
Nous allons commencer par voir une fusion de tables

5
00:00:15,247 --> 00:00:18,908
c'est-à-dire concaténer des enregistrements.

6
00:00:19,008 --> 00:00:21,239
Concaténer des tables, cela veut dire rajouter des enregistrements.

7
00:00:23,986 --> 00:00:26,171
Nous avons Pierre et Marie

8
00:00:26,271 --> 00:00:29,135
qui ont leur carnet de contacts

9
00:00:29,235 --> 00:00:30,784
avec un certain nombre d'informations.

10
00:00:30,884 --> 00:00:33,414
Ici, il y a quatre descripteurs

11
00:00:33,514 --> 00:00:35,392
nom, prénom, téléphone et email.

12
00:00:35,850 --> 00:00:39,168
Marie a les mêmes descripteurs,

13
00:00:39,268 --> 00:00:40,382
pas forcément dans le même ordre.

14
00:00:40,482 --> 00:00:42,930
On verra que pour l'instant, ce n'est pas du tout gênant.

15
00:00:43,030 --> 00:00:45,705
Et avec des enregistrements.

16
00:00:45,805 --> 00:00:47,170
Alors certains sont communs

17
00:00:47,910 --> 00:00:49,666
et certains non.

18
00:00:49,766 --> 00:00:52,703
Et l'idée, c'est de construire un seul carnet

19
00:00:52,803 --> 00:00:54,127
avec les enregistrements.

20
00:00:54,227 --> 00:00:56,873
Bien sûr, on ne va pas dupliquer,

21
00:00:56,973 --> 00:01:00,816
on voudrait que les enregistrements qui se trouvent être les mêmes

22
00:01:00,916 --> 00:01:03,917
ne se retrouvent qu'une fois dans la table commune.

23
00:01:04,495 --> 00:01:07,345
Déjà, c'est une première difficulté

24
00:01:07,557 --> 00:01:11,388
il faut bien faire attention que dès qu'on va travailler avec plusieurs tables

25
00:01:11,488 --> 00:01:16,251
vont arriver plusieurs difficultés potentielles.

26
00:01:16,590 --> 00:01:21,412
J'ai listé quelques-unes de ces difficultés ici.

27
00:01:22,281 --> 00:01:25,290
La première, c'est est-ce que deux enregistrements

28
00:01:25,992 --> 00:01:27,890
représentent effectivement la même information ?

29
00:01:30,389 --> 00:01:33,102
Dans le cas qui nous intéresse, avec nos deux carnets d'adresses,

30
00:01:33,202 --> 00:01:36,242
on voit par exemple ici qu'on a un Derand Michel

31
00:01:36,342 --> 00:01:39,488
qu'on retrouve ici : Michel Derand.

32
00:01:40,044 --> 00:01:42,654
On peut se poser la question : s'agit-il du même Michel Derand ?

33
00:01:42,754 --> 00:01:47,738
Cette question de l'égalité entre deux enregistrements est légitime

34
00:01:48,051 --> 00:01:52,360
surtout si on n'a aucune précision

35
00:01:53,175 --> 00:01:54,244
sur ces enregistrements.

36
00:01:54,344 --> 00:01:55,974
Par exemple ici, on ne sait pas si

37
00:01:56,074 --> 00:01:58,236
le nom et le prénom sont discriminants

38
00:01:58,336 --> 00:01:59,657
pour les enregistrements que nous avons.

39
00:02:00,057 --> 00:02:04,695
On verra dans le module bases de données

40
00:02:06,709 --> 00:02:09,175
qu'en général, les enregistrements

41
00:02:09,275 --> 00:02:11,513
ont un champ supplémentaire

42
00:02:11,613 --> 00:02:13,070
qui est une clé

43
00:02:14,269 --> 00:02:18,530
qui permet justement d'identifier de façon sûre et unique

44
00:02:18,630 --> 00:02:19,869
chacun des enregistrements.

45
00:02:20,267 --> 00:02:23,697
Ici, si on avait une clé, un identifiant

46
00:02:25,556 --> 00:02:27,628
unique pour chaque personne,

47
00:02:27,958 --> 00:02:29,791
imaginons que ce soit par exemple 

48
00:02:29,891 --> 00:02:35,569
le numéro, l'entier qui a été rajouté ici par mon notebook,

49
00:02:35,669 --> 00:02:41,243
donc si 1, c'est l'identifiant correspondant à la personne Derand Michel

50
00:02:41,343 --> 00:02:44,503
et que dans la table de Pierre,

51
00:02:44,603 --> 00:02:46,312
ce 1 soit aussi l'identifiant,

52
00:02:46,412 --> 00:02:48,744
là on serait sûr qu'effectivement

53
00:02:49,485 --> 00:02:50,918
Derand Michel, c'est la même personne

54
00:02:51,018 --> 00:02:52,258
puisqu'il a le même identifiant.

55
00:02:52,822 --> 00:02:55,513
Bien sûr, il faudrait donc que, ici,

56
00:02:56,255 --> 00:02:59,956
l'identifiant 2 corresponde aussi à la même personne,

57
00:03:00,056 --> 00:03:03,509
mais 3 par exemple correspond à la personne Julie Bremond chez Pierre

58
00:03:03,609 --> 00:03:07,880
et il correspond à la personne Luc Menate chez Marie,

59
00:03:07,980 --> 00:03:11,128
ici, nos numéros ne sont que des numéros de ligne

60
00:03:11,228 --> 00:03:14,957
et ne correspondent pas du tout à des identifiants uniques

61
00:03:15,057 --> 00:03:16,393
pour les personnes.

62
00:03:18,235 --> 00:03:23,074
En tout cas, cette notion de clé, et notamment de clé primaire,

63
00:03:23,174 --> 00:03:26,497
va arriver, nous la verrons dans le cours bases de données

64
00:03:26,597 --> 00:03:28,101
mais elle n'est pas présente ici.

65
00:03:28,201 --> 00:03:30,965
Nous avons des tables simples, sans clé.

66
00:03:31,065 --> 00:03:33,663
Il faudra simplement savoir

67
00:03:34,777 --> 00:03:36,315
ce qui correspond à une clé,

68
00:03:36,415 --> 00:03:39,527
c'est-à-dire ce qui discrimine exactement

69
00:03:39,627 --> 00:03:41,520
de façon sûre chacun de nos enregistrements.

70
00:03:41,620 --> 00:03:44,381
Par exemple, on va prendre ici comme clé

71
00:03:44,481 --> 00:03:46,064
le couple nom/prénom.

72
00:03:46,713 --> 00:03:48,779
On va supposer que Michel Derand

73
00:03:48,879 --> 00:03:50,665
et Derand Michel sont la même personne

74
00:03:50,765 --> 00:03:54,345
et que l'on va bien pouvoir fusionner leurs informations.

75
00:03:54,919 --> 00:03:56,876
Ça, c'est la première difficulté.

76
00:03:56,976 --> 00:03:59,818
La deuxième, c'est les éventuels conflits

77
00:03:59,918 --> 00:04:02,190
sur les valeurs d'un même descripteur.

78
00:04:02,290 --> 00:04:03,575
Ici, le cas ne se présente pas

79
00:04:03,675 --> 00:04:06,607
je n'ai pas fait de cas où par exemple

80
00:04:07,570 --> 00:04:11,562
l'email de Derand Michel

81
00:04:11,662 --> 00:04:13,269
serait différent

82
00:04:13,369 --> 00:04:15,648
dans la table de Pierre et dans la table de Marie.

83
00:04:15,748 --> 00:04:17,543
La question se poserait alors de

84
00:04:18,590 --> 00:04:21,346
la validité de l'un ou l'autre des deux mails.

85
00:04:21,446 --> 00:04:23,163
Ou peut-être qu'il faudrait garder les deux, d'ailleurs.

86
00:04:24,346 --> 00:04:25,970
Il n'y a pas ce cas-là qui se présente

87
00:04:26,070 --> 00:04:27,567
mais il faut savoir qu'effectivement,

88
00:04:27,667 --> 00:04:30,183
lorsque l'on va concaténer des tables,

89
00:04:30,945 --> 00:04:33,604
on pourrait être amené à se poser la question

90
00:04:33,704 --> 00:04:38,316
pour un descripteur, on a deux valeurs distinctes présentes

91
00:04:38,416 --> 00:04:43,590
et quelle est la solution qui est adoptée.

92
00:04:45,685 --> 00:04:51,195
Enfin, troisième problème auquel il faudra faire attention

93
00:04:51,295 --> 00:04:53,300
c'est l'ordre des clés

94
00:04:53,400 --> 00:04:55,125
mais ce problème, on en reparlera

95
00:04:55,225 --> 00:04:57,695
puisqu'il va surtout intervenir au moment de l'écriture du fichier.

96
00:04:58,844 --> 00:05:03,134
Donc le but, c'est de créer un fichier de contacts commun

97
00:05:03,234 --> 00:05:05,396
avec les contacts de Pierre et les contacts de Marie.

