1
00:00:01,611 --> 00:00:04,892
Rappelons ici : nous avions un dictionnaire commun

2
00:00:04,992 --> 00:00:09,033
avec trois entrées, les trois entrées de Pierre

3
00:00:09,133 --> 00:00:11,233
et nous avons fusionné,

4
00:00:11,333 --> 00:00:13,321
nous nous retrouvons maintenant avec un dictionnaire

5
00:00:13,421 --> 00:00:17,056
qui contient toutes les entrées

6
00:00:17,948 --> 00:00:21,533
les trois entrées de Pierre

7
00:00:21,633 --> 00:00:24,667
plus, non pas les quatre de Marie

8
00:00:24,767 --> 00:00:27,304
puisque certaines sont communes,

9
00:00:27,404 --> 00:00:30,548
Michel et Anaïs sont deux entrées communes

10
00:00:30,648 --> 00:00:32,231
qui n'ont pas été dupliquées.

11
00:00:32,826 --> 00:00:34,560
Et dans le dictionnaire commun évidemment

12
00:00:34,660 --> 00:00:40,163
nous espérons avoir gardé le numéro de téléphone de Michel

13
00:00:40,263 --> 00:00:42,612
qui était présent dans les contacts de Pierre

14
00:00:44,776 --> 00:00:46,375
et l'email d'Anaïs

15
00:00:46,475 --> 00:00:49,735
qui était présent dans les contacts de Marie.

16
00:00:49,835 --> 00:00:52,456
Nous avons normalement, pour Michel et pour Anaïs,

17
00:00:52,556 --> 00:00:54,079
des informations complètes

18
00:00:54,179 --> 00:00:55,407
dans le dictionnaire commun.

19
00:00:55,507 --> 00:00:56,727
Effectivement, c'est le cas ;

20
00:00:56,827 --> 00:00:59,150
si nous regardons l'entrée pour Michel

21
00:00:59,250 --> 00:01:00,555
nous avons toutes les informations,

22
00:01:00,655 --> 00:01:02,048
de même que pour Anaïs.

23
00:01:02,674 --> 00:01:04,583
Donc la fusion s'est effectivement bien passée,

24
00:01:04,683 --> 00:01:06,327
nous avons donc fusionné,

25
00:01:06,427 --> 00:01:08,449
concaténé deux tables,

26
00:01:08,649 --> 00:01:10,336
ici, deux tables simples

27
00:01:10,436 --> 00:01:11,707
mais on pourrait faire la même chose

28
00:01:11,807 --> 00:01:12,847
 avec des tables un peu plus compliquées.

29
00:01:15,206 --> 00:01:19,259
Donc maintenant que nous avons notre structure commune,

30
00:01:19,359 --> 00:01:21,109
évidemment il va falloir écrire

31
00:01:21,916 --> 00:01:23,737
les informations dans un fichier

32
00:01:23,837 --> 00:01:26,249
qui pourra servir ensuite à Pierre et à Marie.

33
00:01:27,846 --> 00:01:31,176
Nous avons écrit une fonction ecrire

34
00:01:31,276 --> 00:01:33,388
qui va prendre un nom de fichier,

35
00:01:33,488 --> 00:01:38,678
le dictionnaire qui stocke les enregistrements,

36
00:01:39,272 --> 00:01:40,716
la liste des descripteurs,

37
00:01:40,816 --> 00:01:42,917
là, c'est important puisque effectivement,

38
00:01:43,017 --> 00:01:44,848
il va falloir écrire les informations

39
00:01:44,948 --> 00:01:45,940
toujours dans le même ordre.

40
00:01:47,674 --> 00:01:51,143
Et enfin le délimiteur et puis l'encodage.

41
00:01:51,243 --> 00:01:54,164
Nous ouvrons notre fichier en écriture

42
00:01:54,264 --> 00:01:56,961
et il va falloir écrire les descripteurs.

43
00:01:57,061 --> 00:01:59,081
Il faut une première ligne

44
00:01:59,181 --> 00:02:01,321
pour écrire les descripteurs.

45
00:02:03,459 --> 00:02:09,553
Ici, nous utilisons simplement la fonction write

46
00:02:09,653 --> 00:02:10,257
sur les fichiers,

47
00:02:10,357 --> 00:02:13,010
nous n'utilisons pas de module particulier

48
00:02:13,110 --> 00:02:14,835
mais évidemment, nous pourrions faire la même chose

49
00:02:14,935 --> 00:02:16,674
en utilisant le module csv.

50
00:02:18,855 --> 00:02:21,670
Donc ici, nous allons écrire une chaîne de caractères

51
00:02:21,770 --> 00:02:26,814
qui va être constituée de l'ensemble des descripteurs

52
00:02:27,468 --> 00:02:30,260
séparés alors ici par un point-virgule.

53
00:02:31,448 --> 00:02:33,638
Nous prenons l'ensemble des descripteurs,

54
00:02:34,717 --> 00:02:37,478
et donc une liste de chaînes de caractères.

55
00:02:37,578 --> 00:02:39,564
Nous pouvons utiliser join sur cette liste

56
00:02:39,664 --> 00:02:41,234
de chaînes de caractères

57
00:02:41,751 --> 00:02:46,480
pour les concaténer en intercalant un point-virgule.

58
00:02:46,908 --> 00:02:48,256
Tout simplement.

59
00:02:48,356 --> 00:02:49,963
Et comme le write ne rajoute pas

60
00:02:50,063 --> 00:02:53,059
contrairement au print de retour à la ligne,

61
00:02:53,159 --> 00:02:56,695
nous allons explicitement mettre

62
00:02:57,557 --> 00:02:59,410
un retour à la ligne.

63
00:03:01,247 --> 00:03:03,821
Voilà pour l'écriture de cette première ligne,

64
00:03:03,921 --> 00:03:05,277
donc l'écriture des descripteurs,

65
00:03:05,377 --> 00:03:09,452
nom, prénom, tel et email comme ici,

66
00:03:10,180 --> 00:03:13,696
et maintenant que nous avons écrit cette première ligne,

67
00:03:13,796 --> 00:03:18,295
il nous faut parcourir l'ensemble des valeurs du carnet

68
00:03:18,395 --> 00:03:23,373
pour écrire chacun des enregistrements en ligne.

69
00:03:23,913 --> 00:03:26,672
Donc une petite boucle sur,

70
00:03:26,772 --> 00:03:31,053
on va les appeler enregistrement,

71
00:03:31,522 --> 00:03:32,898
puisque c'en sont,

72
00:03:38,194 --> 00:03:41,811
nous parcourons les valeurs du carnet.

73
00:03:47,459 --> 00:03:51,658
Et ici, nous allons donc également faire un write

74
00:03:52,367 --> 00:03:53,582
Qu'est-ce que nous voulons écrire ?

75
00:03:53,682 --> 00:03:54,979
Quelle ligne voulons-nous former ?

76
00:03:55,079 --> 00:03:57,098
Alors toujours séparés par des points-virgules

77
00:03:57,198 --> 00:03:58,714
nous allons évidemment utiliser le join

78
00:03:58,814 --> 00:03:59,937
qui est quand même bien pratique

79
00:04:00,037 --> 00:04:02,207
et ce que nous voulons, c'est

80
00:04:02,914 --> 00:04:07,581
obtenir les valeurs de l'enregistrement

81
00:04:09,057 --> 00:04:10,782
Ici, j'ai écrit un peu n'importe quoi

82
00:04:10,882 --> 00:04:17,973
for enregistrement in d_carnet.values()

83
00:04:18,073 --> 00:04:19,587
ce que nous voulons, c'est

84
00:04:20,284 --> 00:04:22,755
l'enregistrement, je vous rappelle, c'est un dictionnaire

85
00:04:24,760 --> 00:04:27,080
nous voulons la valeur de

86
00:04:29,060 --> 00:04:32,240
de la clé pour ce dictionnaire.

87
00:04:33,039 --> 00:04:34,960
Et la clé, où est-ce que nous la prenons ?

88
00:04:35,609 --> 00:04:39,501
Nous la prenons évidemment parmi les descripteurs.

89
00:04:43,222 --> 00:04:46,158
Et le fait d'avoir cette liste de descripteurs

90
00:04:46,258 --> 00:04:51,120
et de prendre la clé toujours dans cette liste

91
00:04:51,220 --> 00:04:54,016
garantit que nous prenons effectivement les clés

92
00:04:54,116 --> 00:04:56,365
dans le même ordre pour tous les enregistrements.

93
00:04:56,951 --> 00:04:58,927
Ce qui ne serait pas le cas

94
00:04:59,027 --> 00:05:03,904
si nous faisions for cle in enregistrement.

95
00:05:07,926 --> 00:05:09,504
Puisque enregistrement est un dictionnaire,

96
00:05:09,604 --> 00:05:10,605
nous pourrions faire cela

97
00:05:10,705 --> 00:05:12,681
mais nous n'avons pas de garantie

98
00:05:12,781 --> 00:05:15,198
que l'ordre serait le même.

99
00:05:15,660 --> 00:05:18,187
Et ça, c'est important puisque effectivement

100
00:05:18,287 --> 00:05:20,885
quand on écrit chacun des enregistrements dans le fichier

101
00:05:20,985 --> 00:05:24,149
nous devons avoir nos champs

102
00:05:24,911 --> 00:05:27,119
qui sont dans le même ordre pour tous les enregistrements.

103
00:05:29,620 --> 00:05:32,763
Donc ici, il faut bien mettre descripteurs.

104
00:05:34,182 --> 00:05:38,193
Et rajouter le passage à la ligne.

105
00:05:38,913 --> 00:05:40,310
Nous avons juste défini la fonction

106
00:05:40,410 --> 00:05:43,813
et donc nous allons l'appeler sur commun.csv

107
00:05:44,700 --> 00:05:48,660
pour créer le fichier effectivement

108
00:05:58,204 --> 00:05:59,209
on le voit apparaître,

109
00:06:00,590 --> 00:06:01,625
il est là

110
00:06:03,913 --> 00:06:08,713
et nous avons bien notre fichier commun

111
00:06:08,813 --> 00:06:11,384
avec les enregistrements communs à Pierre et à Marie.

112
00:06:13,237 --> 00:06:18,517
Nous allons dans une seconde vidéo

113
00:06:18,617 --> 00:06:21,183
sur la manipulation multiple de tables,

114
00:06:21,283 --> 00:06:26,180
voir comment nous pouvons fusionner des tables,

115
00:06:26,280 --> 00:06:27,267
faire des jointures en fait,

116
00:06:27,367 --> 00:06:29,773
pour ajouter non pas des enregistrements

117
00:06:29,873 --> 00:06:32,854
mais des champs.

