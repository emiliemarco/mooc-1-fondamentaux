## Présentation du module : Traitement des données en tables

## Objectifs du module 

Dans ce module, nous allons manipuler des données stockées dans des fichiers texte : ce sont les données en table.

### Prérequis

Module 1.2 sur les types construits et bien sûr un minimum de connaissance du langage Python.

### Sommaire

- des explications écrites à propos :
   - des formats de fichiers : texte vs binaire, csv
   - des moyens de lire et charger dans un script les données stockées en table
   - des opérations que l'on peut faire sur ces données
   - des facilités offertes par les modules csv et pandas (sans entrer dans l'étude détaillée de ceux-ci)
- des vidéos qui se focalisent sur les traitements de données en table ; ces vidéos peuvent être visionnées indépendamment des textes


### Temps d'investissement

Entre 6 et 10 heures en fonction de votre aisance avec Python.

## Enseignant

### Sébastien Hoarau

Maitre de Conférence en Informatique à l’Université de la Réunion et membre de l'IREM de la Réunion. Il enseigne l’initiation à la programmation impérative avec le langage Python3 à des 1ère années scientifiques. Initiation aux techno du web aussi : HTML, CSS, JavaScript
