# Manipulations d'objets

Le but de cet exercice en plusieurs questions est de manipuler quelques objets simples. Ce seront des _playlists_ de titres de musique et des durées. Après avoir réalisé l'exercice, vous répondrez aux questions du quiz qui suit.

## Objet `Duree`

On souhaite manipuler des durées sous la forme _heure_, _minutes_ et _secondes_. 

### Question 1

Créer un objet `Duree` comportant les attributs `heure`, `min` et `sec`. Voici quelques exemples de création d'instances de cet objet :

```python
>>> d0 = Duree(5, 34, 12)
>>> d1 = Duree(0, 70, 60)
>>> d1
Duree(1, 11, 0)
```

On vous donne la méthode `__repr__` (celle qui est invoquée lorsqu'on demande à l'interprète de nous évaluer l'instance `d1` ci-dessus par exemple) :

```python
def __repr__(self):
    return f'Duree({self.heure}, {self.min:02}, {self.sec:02})'
```

### Question 2

Il nous faudra pouvoir comparer des durées. Écrire les trois méthodes pour les opérateurs supérieur, inférieur et égal :

```python
def __gt__(self, autre_duree):
    # à compléter

def __lt__(self, autre_duree):
    # à compléter

def __eq__(self, autre_duree):
    # à compléter
```

### Question 3

Écrire la méthode `__add__` permettant d'additionner des durées : `d0 + d1` doit créer une 3e durée qui est la somme des durées `d0` et `d1` :

```python
>>> Duree(0, 0, 34) + Duree(0, 0, 52)
Duree(0, 1, 26)
```

### Question subsidiaire

Dans la suite, on aura besoin de créer une durée via une chaîne de caractères de la forme `heures:minutes:secondes`. On pourra définir une méthode de classe `duree`  qui accepte une chaîne de caractères représentant des heures, des minutes et des secondes séparées par un caractère. Par exemple :

```python
>>> Duree.duree('6:19:20')
Duree(6, 19, 20)
>>> Duree.duree('0:234:7665')
Duree(6, 1, 45)
```

_Note_ : le début de la définition de cette méthode est donnée dans les indications disponibles en bas de la page. 


## Objet `Titre` 

Un titre musical est un objet qui comporte les attributs suivants :

- `titre` : le titre du morceau, il s'agit d'une chaîne de caractères
- `album` : le nom de l'album dont est extrait le morceau (également une chaîne de caractères)
- `annee` : l'année de sortie de l'album (toujours une chaînes de caractères)
- `duree` : la durée du morceau (un objet `Duree`)

Xuan, Bob et Inaya sont trois ami-es qui ont en commun de n'écouter que des titres du groupe [Pink Floyd](https://fr.wikipedia.org/wiki/Pink_Floyd).

Le fichier csv [`pink_floyd_durees.csv`](https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/blob/master/__supports-de-cours/B2-M1/pink_floyd_durees.csv) contient quelques titres du célèbre groupe de rock. Téléchargez ce fichier.

### Question 4 

À partir de ce fichier, créez la constante `PINK_FLOYD` qui contient la liste des objets `Titre` correspondant aux lignes du fichier, dans le même ordre.

### Question 5

Une `PlayList` est constituée d'une `base` (une liste de `Titre`) et d'une `ids`, ensemble (au sens `set` de Python) d'indices, correspondant aux titres de la `base` présents dans la _playlist_.

Définir la classe `PlayList`. Y ajouter les méthodes suivantes :

- `titre` : qui prend un numéro de titre (un _id_) et qui renvoie le `Titre` (au sens objet) correspondant de la _playlist_
- `duree` : qui envoie la durée totale (sous la forme d'un objet `Duree`) de la _playlist_
- `commun` : qui prend une autre `PlayList` en paramètre et crée une troisième `PlayList` constituée des `Titres` que les deux _playlists_ ont en commun.
- `__str__` qui permet d'afficher une `PlayList` ; comme ceci (entre parenthèses on trouve le nom de l'album et l'année) :

    ```python
    >>> small = PlayList(PINK_FLOYD, {45, 32, 120})
    >>> print(small)
    ```

    ```
     32 [01:06] Unsung (The Endless River, 2014)
    120 [12:48] A Saucerful of Secrets (Ummagumma, 1969)
     45 [03:19] In the Flesh ? (The Wall, 1979)
    ```

### Question 6

Ci-dessous les _ids_ de Xuan, Bob et Inaya :

```python
XUAN = {60, 45, 107, 10, 51, 5, 30, 83, 94, 22, 4, 136, 145, 52, 133, 125, 86, 31, 87, 118, 82, 32, 43, 3, 27, 97, 150, 79, 152, 114, 54, 53, 93, 80, 141, 18, 115, 105, 72, 142, 81, 149, 17, 104, 102, 39, 11, 36, 91, 147, 134, 84, 117, 15, 128, 89, 50, 113, 33, 61, 124, 23, 59, 40, 111, 26, 100, 112, 19, 135, 123, 44, 119, 62, 155, 78, 7, 110, 157, 98, 99, 34, 65, 58, 139, 77, 47, 46, 8, 88, 1, 49, 95, 16, 41}

BOB = {129, 134, 58, 65, 67, 0, 102, 140, 74, 34, 85, 73, 28, 40, 56, 101, 12, 25, 35, 68, 39, 55, 124, 37, 26, 49, 59, 146, 108, 36, 106, 21, 3, 117, 123, 143, 100, 64, 9, 22, 156, 76, 19, 4, 122, 79, 109, 62, 113, 142, 89, 152, 1, 128, 43, 81, 126, 94, 135, 118, 7, 136, 141, 93, 11, 114, 20, 95, 155, 53, 138, 42, 2, 78, 61, 10, 32, 132, 5, 110, 125, 72, 45, 111, 92, 88, 145, 121, 149, 150, 51, 41, 57, 69, 98, 63, 104, 47, 90, 48, 24, 120, 31, 130, 70}

INAYA = {57, 41, 29, 83, 72, 2, 38, 25, 132, 60, 14, 136, 140, 127, 152, 54, 13, 17, 16, 116, 119, 101, 133, 129, 95, 130, 18, 63, 15, 64, 156, 52, 39, 123, 10, 73, 157, 107, 7, 58, 103, 75, 154, 61, 86, 137, 87, 111, 12, 47, 24, 23, 8, 117, 35, 108, 150, 118, 44, 42, 26, 55, 3, 32, 30, 59, 97, 74, 67, 99, 69, 88, 135, 131, 46, 53, 128, 112, 145, 125, 82, 147, 71, 68, 93, 113, 36, 4, 141, 65, 40, 50, 20, 94, 19, 122, 56, 11, 49, 76, 34, 153, 79, 9, 0, 5, 28, 70, 138, 151, 110, 144, 77, 33, 22, 100, 90, 143, 155, 126}
```

1. Créer `xuan`, `bob` et `inaya`, les `PlayList` des trois ami-es.
2. Créer la `PlayList` commune aux trois personnes.
3. Créer la `PlayList` des morceaux de Bob que ne possèdent pas ses deux amies.
