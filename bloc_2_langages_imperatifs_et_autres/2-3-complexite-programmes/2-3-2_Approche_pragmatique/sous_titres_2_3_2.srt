1
00:00:00,367 --> 00:00:02,158
Une première approche pragmatique

2
00:00:02,258 --> 00:00:04,295
pour avoir des informations sur le temps d'exécution

3
00:00:04,395 --> 00:00:05,940
est tout simplement de le mesurer

4
00:00:06,040 --> 00:00:07,133
sur différents jeux de données.

5
00:00:07,511 --> 00:00:09,792
Le module timeit permet de réaliser

6
00:00:09,892 --> 00:00:11,944
plusieurs exécutions d'un code donné

7
00:00:12,044 --> 00:00:13,856
et de retenir le meilleur temps CPU

8
00:00:13,956 --> 00:00:15,432
parmi l'ensemble des exécutions.

9
00:00:15,532 --> 00:00:18,044
Les exécutions peuvent se faire au terminal,

10
00:00:18,144 --> 00:00:20,418
dans un script ou une console Python.

11
00:00:20,518 --> 00:00:22,081
Par exemple, dans un terminal,

12
00:00:22,181 --> 00:00:25,970
l'instruction python3 -m timeit -n 5

13
00:00:26,070 --> 00:00:27,500
avec le code  print('coucou')

14
00:00:27,600 --> 00:00:28,847
mesure le temps d'exécution

15
00:00:28,947 --> 00:00:30,519
pris pour exécuter cinq fois

16
00:00:30,619 --> 00:00:31,754
le code donné en paramètre

17
00:00:31,854 --> 00:00:32,850
comme montré ici.

18
00:00:33,560 --> 00:00:36,901
Ou ici, à la console en utilisant le module timeit,

19
00:00:37,001 --> 00:00:38,559
un objet de la classe Timer

20
00:00:38,659 --> 00:00:39,805
et la méthode timeit.

21
00:00:41,300 --> 00:00:42,668
Prenons un autre exemple.

22
00:00:42,768 --> 00:00:45,146
Supposons que nous désirions analyser

23
00:00:45,759 --> 00:00:48,357
les deux fonctions anagramme_dico

24
00:00:48,457 --> 00:00:50,096
avec les paramètres a, b,

25
00:00:50,196 --> 00:00:51,410
qui sont des chaînes de caractères,

26
00:00:51,510 --> 00:00:52,879
et anagramme_list,

27
00:00:54,200 --> 00:00:56,590
données ici qui renvoient toutes deux

28
00:00:56,690 --> 00:00:59,804
True si les séquences a et b

29
00:00:59,904 --> 00:01:02,081
sont des permutations l'une de l'autre.

30
00:01:03,432 --> 00:01:05,764
Prenons initialement une chaîne de caractères

31
00:01:05,864 --> 00:01:07,442
de dix caractères

32
00:01:07,709 --> 00:01:11,796
dont on multiplie la longueur par 2 à chaque itération.

33
00:01:11,896 --> 00:01:14,705
Et donnons le temps d'exécution de chaque code.

34
00:01:15,700 --> 00:01:17,471
On voit dans le tableau que

35
00:01:17,571 --> 00:01:20,422
l'exécution est effectivement presque immédiate

36
00:01:20,522 --> 00:01:21,871
pour les chaînes de caractères

37
00:01:21,971 --> 00:01:24,441
de taille inférieure à cent mille caractères

38
00:01:24,541 --> 00:01:29,183
mais au-delà, la fonction anagramme_list

39
00:01:29,283 --> 00:01:31,625
prend un temps de plus en plus significatif.

40
00:01:32,787 --> 00:01:34,091
Par exemple,

41
00:01:34,191 --> 00:01:37,541
la dernière ligne du tableau ici,

42
00:01:38,105 --> 00:01:40,987
pour un peu plus de cinq millions de caractères,

43
00:01:41,087 --> 00:01:43,105
il faut trois mille secondes

44
00:01:43,205 --> 00:01:45,058
soit près d'une heure

45
00:01:45,158 --> 00:01:46,189
pour la version liste,

46
00:01:46,289 --> 00:01:47,513
quand la version dictionnaire

47
00:01:47,613 --> 00:01:48,715
prend moins d'une seconde.

48
00:01:49,147 --> 00:01:51,488
On peut estimer grossièrement que le temps d'exécution

49
00:01:51,588 --> 00:01:54,343
grandit linéairement par rapport à la taille des séquences

50
00:01:54,443 --> 00:01:56,161
avec la fonction anagramme_dico

51
00:01:56,261 --> 00:01:59,148
et de façon quadratique par rapport à la taille des séquences

52
00:01:59,248 --> 00:02:01,222
avec la fonction anagramme_list.

53
00:02:01,642 --> 00:02:03,054
Il est intuitivement normal

54
00:02:03,154 --> 00:02:04,616
que ces fonctions prennent un temps

55
00:02:04,716 --> 00:02:06,541
proportionnel à la taille de a,

56
00:02:07,168 --> 00:02:08,151
on suppose évidemment

57
00:02:08,251 --> 00:02:10,022
que les deux chaînes de caractères ont la même taille.

58
00:02:10,122 --> 00:02:12,242
Par contre, cette démarche est chronophage

59
00:02:12,342 --> 00:02:13,837
et on atteint vite ses limites

60
00:02:13,937 --> 00:02:15,951
car il n'est pas possible de faire

61
00:02:16,051 --> 00:02:18,881
des tests pour tous les exemples intéressants possibles

62
00:02:18,981 --> 00:02:20,984
et sur l'ensemble des ordinateurs possibles.

