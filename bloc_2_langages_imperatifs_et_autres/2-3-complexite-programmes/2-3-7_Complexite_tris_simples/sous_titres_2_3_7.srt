1
00:00:00,000 --> 00:00:01,381
Donnons ici

2
00:00:01,481 --> 00:00:02,954
la complexité maximale

3
00:00:03,054 --> 00:00:04,417
en terme de grand O

4
00:00:04,517 --> 00:00:06,067
des tris classiques, c'est-à-dire

5
00:00:06,167 --> 00:00:07,352
le tri par sélection,

6
00:00:07,452 --> 00:00:08,404
le tri par insertion,

7
00:00:08,504 --> 00:00:09,876
le tri par échange ou tri bulle

8
00:00:09,976 --> 00:00:11,913
et le tri par énumération.

9
00:00:12,351 --> 00:00:15,139
Soit une solution Python du tri par sélection.

10
00:00:15,614 --> 00:00:17,217
On suppose ici que les valeurs s

11
00:00:17,317 --> 00:00:20,110
sont composées de tuples,

12
00:00:20,444 --> 00:00:21,449
de couples en fait,

13
00:00:21,549 --> 00:00:24,297
avec la composante 0 qui correspond à la clé

14
00:00:24,397 --> 00:00:28,079
et la composante 1 qui correspond aux informations satellites.

15
00:00:28,526 --> 00:00:31,547
Pour calculer la complexité maximale du tri par sélection,

16
00:00:31,647 --> 00:00:34,174
décomposons implicitement ou explicitement

17
00:00:34,274 --> 00:00:36,707
l'algorithme sous forme d'arbre syntaxique

18
00:00:36,807 --> 00:00:38,275
en suivant sa structure.

19
00:00:39,539 --> 00:00:41,432
En supposant que toutes les instructions

20
00:00:41,532 --> 00:00:42,619
et traitements de passage de paramètres

21
00:00:42,719 --> 00:00:44,084
interviennent dans le calcul,

22
00:00:44,184 --> 00:00:46,891
et en suivant les règles pour calculer cette complexité,

23
00:00:46,991 --> 00:00:52,400
nous obtenons, pour les instructions 1, 5, 8, 11 et 13,

24
00:00:52,977 --> 00:00:54,782
des complexités en O(1).

25
00:00:54,882 --> 00:00:58,345
Le if de l'instruction 10-11 est en O(1),

26
00:00:58,788 --> 00:01:01,989
et le for imbriqué en 9-11

27
00:01:02,089 --> 00:01:04,089
est en O(n).

28
00:01:05,379 --> 00:01:07,000
Le for global

29
00:01:07,344 --> 00:01:09,241
est en O de n carré

30
00:01:09,341 --> 00:01:12,515
puisqu'il y a n - 1 itérations,

31
00:01:12,615 --> 00:01:14,532
et donc le tri par sélection

32
00:01:14,632 --> 00:01:17,571
est en O de n carré en complexité maximale.

33
00:01:18,843 --> 00:01:21,255
Alors ces calculs peuvent être raffinés

34
00:01:21,355 --> 00:01:23,703
mais en fait, on se rend compte que les résultats

35
00:01:23,803 --> 00:01:26,767
pour la complexité maximale restent en O de n carré.

36
00:01:28,000 --> 00:01:30,165
Regardons maintenant le tri par insertion.

37
00:01:30,664 --> 00:01:33,504
On peut voir que la complexité maximale

38
00:01:33,604 --> 00:01:35,578
est également en O de n carré.

39
00:01:36,770 --> 00:01:39,813
Alors ici, la complexité minimale

40
00:01:40,479 --> 00:01:43,725
est rencontrée lorsque la liste est déjà triée

41
00:01:43,825 --> 00:01:45,860
et on peut voir que, dans ce cas-là,

42
00:01:45,960 --> 00:01:47,305
elle est en O(n).

43
00:01:50,421 --> 00:01:53,745
Si je regarde la complexité du tri par échange ou tri bulle,

44
00:01:54,145 --> 00:01:56,602
de nouveau, on se rend compte

45
00:01:56,702 --> 00:01:58,774
que, après analyse,

46
00:01:58,874 --> 00:02:02,586
on a une complexité qui est en O de n carré,

47
00:02:02,989 --> 00:02:04,859
puisqu'il y a n - 1 phases

48
00:02:04,959 --> 00:02:07,859
et que chaque phase est de l'ordre de O(n).

49
00:02:08,577 --> 00:02:10,504
Si par contre, je prends le tri par énumération

50
00:02:10,604 --> 00:02:11,900
je suppose que les clés

51
00:02:12,000 --> 00:02:16,161
sont dans l'intervalle 0..m - 1

52
00:02:16,261 --> 00:02:17,539
donc des valeurs entières,

53
00:02:18,041 --> 00:02:19,875
avec m valeurs possibles donc,

54
00:02:20,336 --> 00:02:22,880
on se rend compte que le tri par énumération

55
00:02:22,980 --> 00:02:24,100
qui procède par phases

56
00:02:24,601 --> 00:02:26,444
a un certain nombre de phases

57
00:02:26,544 --> 00:02:28,135
qui sont soit en O(n),

58
00:02:28,235 --> 00:02:29,625
soit en O(m)

59
00:02:29,974 --> 00:02:31,065
et donc ici,

60
00:02:31,165 --> 00:02:34,646
en supposant que le nombre de possibilités m

61
00:02:34,746 --> 00:02:38,308
est inférieur au nombre d'éléments de la liste,

62
00:02:38,408 --> 00:02:40,383
la complexité maximale

63
00:02:40,483 --> 00:02:41,904
et moyenne de ce tri

64
00:02:42,004 --> 00:02:44,436
sont toutes les deux en O(n)

65
00:02:44,536 --> 00:02:45,761
ce qui est remarquable.

