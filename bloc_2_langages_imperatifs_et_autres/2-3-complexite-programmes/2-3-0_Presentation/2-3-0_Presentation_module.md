## Objectifs du module

Introduire la notion de complexité d'un algorithme et de grand O().  Illustrer ces concepts sur des petits programmes Python.

## Prérequis

Savoir programmer en Python
Avoir les bases de la programmation impérative Python vous sont acquises. La matière correspond au contenu du MOOC "Apprendre à coder avec Python".

## Présentation des séquences / sommaire

## Enseignant

### Thierry Massart

Thierry Massart est professeur à l'Université Libre de Bruxelles (ULB) où, depuis plus de 25 ans, il enseigne la programmation principalement aux étudiants de Sciences Informatique et de l'école Polytechnique de l'ULB..
