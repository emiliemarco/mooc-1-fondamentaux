## Parlons d'efficacité d'un programme

### Motivation


Montrer qu'une version d'un programme est plus *efficace* qu'une autre
n'est, a priori, pas aisé. La succession ou l'imbrication des tests
et des boucles et la multitude des possibilités fait qu'il n'est
généralement pas possible ou raisonnable d'évaluer l'efficacité de
l'algorithme pour chaque cas possible.

### Critères de choix d'un algorithme

Généralement, il existe plusieurs méthodes pour résoudre un même
problème. Il faut alors en choisir une et concevoir un algorithme pour cette
méthode de telle sorte que cet algorithme satisfasse le mieux possible aux
exigences. 

Parmi les critères de sélection d'une méthode et en conséquence d'un algorithme,
deux critères prédominent: la *simplicité* et l'*efficacité* de cet
algorithme. 

Si parfois ces critères vont de paire, bien souvent ils sont contradictoires;
un algorithme efficace est, en effet, bien souvent compliqué et fait appel à
des méthodes  fort élaborées. Le concepteur doit alors choisir entre la
simplicité et l'efficacité. 

Si l'algorithme devra être mis en oeuvre sur un nombre limité de données qui
ne demanderont qu'un nombre limité de calculs, cet algorithme doit de
préférence être simple. En effet un algorithme simple est plus facile à
concevoir et a moins de chance d'être erroné que ne le sera un algorithme
complexe. 

Si, par contre, un algorithme est fréquemment exécuté pour une masse
importante de données, il doit être le plus efficace possible.

Au lieu de parler de l'efficacité d'un
algorithme, on parlera généralement de la notion opposée, à savoir, de la
complexité d'un algorithme.

La complexité d'un algorithme ou d'un programme peut être mesurée de diverses
façons. Généralement, le temps d'exécution est la mesure principale de
la complexité d'un algorithme. 

D'autres mesures sont possibles dont:

  - la quantité d'espace mémoire occupée par le programme et en
    particulier par ses variables;
  - la quantité d'espace disque nécessaire pour que le
    programme s'exécute;
  - la quantité d'information qui doit être transférée (par lecture ou
    écriture) entre le programme et les disques ou entre le programme et des serveurs externes via un réseau;
  -  ...

Nous n'allons pas ici considérer de programme qui échange un grand nombre
d'informations avec des disques ou un autre ordinateur. 

En général nous analyserons le temps d'exécution d'un programme pour évaluer
sa complexité. La mesure de l'espace mémoire occupé par les variables sera
un autre critère qui pourra être utilisé ici.

Nous verrons dans ce qui suit que, généralement, la complexité d'un algorithme est exprimée par
une fonction qui dépend de la taille du problème à résoudre.
