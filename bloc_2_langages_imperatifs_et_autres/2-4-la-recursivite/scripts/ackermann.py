def ackermann(m,n):
    """
    Renvoie la fonction d'Ackermann en (m,n)
    :param m: (int ≥ 0) premier paramètre de la fonction
    :param n: (int ≥ 0) second  paramètre de la fonction
    :return: valeur de la fonction d'Ackermann pour les paramètres donnés
    """
    if(m == 0):
        res = n+1
    elif(m>0 and n ==0):
        res = ackermann(m-1,1)
    else: # m>0, n>0
        res = ackermann(m-1, ackermann(m, n-1))
    return res

