1
00:00:00,642 --> 00:00:02,616
Il existe de nombreux autres langages

2
00:00:02,716 --> 00:00:03,732
qui n'entrent pas parfaitement

3
00:00:03,832 --> 00:00:04,700
dans l'une des catégories

4
00:00:04,800 --> 00:00:07,278
impératifs, orientés objet ou déclaratifs.

5
00:00:07,603 --> 00:00:09,643
L'entrée Paradigmes de programmation

6
00:00:09,743 --> 00:00:11,830
section Autres types de Wikipedia

7
00:00:11,930 --> 00:00:13,945
donne des dizaines de paradigmes différents.

8
00:00:14,045 --> 00:00:15,396
Citons en particulier

9
00:00:15,644 --> 00:00:17,483
la programmation événementielle,

10
00:00:17,583 --> 00:00:19,640
où un programme sera principalement défini

11
00:00:19,740 --> 00:00:20,702
par ses réactions

12
00:00:21,145 --> 00:00:22,847
aux différents événements

13
00:00:22,947 --> 00:00:23,815
qui peuvent se produire,

14
00:00:23,915 --> 00:00:26,300
par exemple, Tcl/Tk gère un tel paradigme ;

15
00:00:26,400 --> 00:00:27,913
la programmation concurrente

16
00:00:28,013 --> 00:00:30,057
où plusieurs processus appelés threads ou tâches

17
00:00:30,157 --> 00:00:31,495
s'exécutent en concurrence,

18
00:00:31,595 --> 00:00:34,445
donnant lieu à de nombreux problèmes informatiques complexes

19
00:00:34,545 --> 00:00:37,675
dont les célèbres problèmes d'interblocage et de famines.

20
00:00:37,963 --> 00:00:39,464
De nombreux langages  dédiés,

21
00:00:39,564 --> 00:00:41,487
domain-specific languages en anglais,

22
00:00:41,587 --> 00:00:43,378
sont conçus pour répondre aux contraintes

23
00:00:43,478 --> 00:00:45,010
d'un domaine d'application précis,

24
00:00:45,181 --> 00:00:47,108
on peut citer comme simples exemples

25
00:00:47,208 --> 00:00:48,290
parmi beaucoup d'autres,

26
00:00:48,390 --> 00:00:50,320
les langages de requêtes comme SQL,

27
00:00:50,420 --> 00:00:51,681
les langages de scripts

28
00:00:51,781 --> 00:00:55,718
comme Perl, Raku, Ruby, Lua, Tcl/tK,

29
00:00:55,818 --> 00:00:57,852
les langages d'analyse statistique comme R,

30
00:00:57,952 --> 00:00:59,938
les langages de calcul numérique

31
00:01:00,038 --> 00:01:01,531
comme Matlab ou Mathematica,

32
00:01:01,631 --> 00:01:02,821
les langages de calcul formel

33
00:01:02,921 --> 00:01:05,126
comme Maple, Maxima, Reduce, Derive,

34
00:01:05,226 --> 00:01:06,040
et cætera.

35
00:01:06,936 --> 00:01:08,057
Notons enfin

36
00:01:08,157 --> 00:01:10,390
que souvent les langages sont multi-paradigmes.

37
00:01:10,953 --> 00:01:13,476
Ainsi, Python est un langage impératif,

38
00:01:13,576 --> 00:01:17,435
orienté objet et permet une certaine programmation fonctionnelle.

39
00:01:17,711 --> 00:01:19,191
Si l'on regarde l'ébauche d'article

40
00:01:19,291 --> 00:01:22,545
Comparaison des langages de programmation multi-paradigmes

41
00:01:22,645 --> 00:01:23,492
sur Wikipedia,

42
00:01:23,911 --> 00:01:25,877
dix paradigmes sont cités pour Python ;

43
00:01:25,977 --> 00:01:27,243
le langage Julia

44
00:01:27,343 --> 00:01:29,278
en a plus de dix-sept.

