1
00:00:00,351 --> 00:00:01,579
Pour rédiger cette partie

2
00:00:01,679 --> 00:00:02,942
qui résume brièvement l'histoire

3
00:00:03,042 --> 00:00:05,168
et quelques paradigmes des langages de programmation,

4
00:00:05,268 --> 00:00:07,081
nous nous sommes basés sur les supports

5
00:00:07,181 --> 00:00:08,575
donnés en fin de cette vidéo

6
00:00:08,675 --> 00:00:09,988
et dans le support de notes.

7
00:00:10,395 --> 00:00:12,950
La genèse des tout premiers langages de programmation

8
00:00:13,050 --> 00:00:15,974
avec Fortran, Cobol, Algol et Lisp par exemple

9
00:00:16,074 --> 00:00:18,470
a montré que leur évolution allait être très rapide.

10
00:00:19,604 --> 00:00:21,793
Les versions, voire générations, différentes

11
00:00:21,893 --> 00:00:23,072
se succédèrent rapidement.

12
00:00:24,212 --> 00:00:26,693
De plus, les dialectes se multiplient

13
00:00:26,793 --> 00:00:28,904
au gré du développement de compilateurs,

14
00:00:29,004 --> 00:00:30,758
de bibliothèques et runtime associés.

15
00:00:32,260 --> 00:00:34,036
Ce qui est remarquable également,

16
00:00:34,136 --> 00:00:35,895
c'est le foisonnement de nouveaux langages

17
00:00:35,995 --> 00:00:37,425
qui sont apparus rapidement

18
00:00:37,525 --> 00:00:39,579
presque toujours comme héritiers manifestes

19
00:00:39,679 --> 00:00:41,345
d'un ou plusieurs prédécesseurs

20
00:00:41,445 --> 00:00:44,669
que cette filiation soit annoncée et acceptée

21
00:00:44,769 --> 00:00:45,403
ou non.

22
00:00:46,535 --> 00:00:49,622
Ceci explique la grande similitude entre langages,

23
00:00:49,722 --> 00:00:50,885
c'est aussi une grande chance

24
00:00:50,985 --> 00:00:52,156
pour les candidats programmeurs

25
00:00:52,256 --> 00:00:53,797
lors de l'étude d'un nouveau langage.

26
00:00:54,264 --> 00:00:56,185
Il existe de nombreux auteurs

27
00:00:56,285 --> 00:00:57,728
qui proposent une généalogie

28
00:00:57,828 --> 00:00:59,271
des différents langages usuels.

29
00:00:59,723 --> 00:01:02,006
Au-delà de cette visualisation historique

30
00:01:02,106 --> 00:01:03,751
des créations de langages,

31
00:01:03,851 --> 00:01:06,220
il est utile d'essayer de les regrouper par famille

32
00:01:06,320 --> 00:01:07,970
présentant un grand ensemble

33
00:01:08,070 --> 00:01:09,340
de caractéristiques communes.

34
00:01:09,440 --> 00:01:12,572
Par exemple, Fortran, défini en 1956-57,

35
00:01:12,672 --> 00:01:14,922
visait à réaliser des calculs scientifiques.

36
00:01:15,022 --> 00:01:16,470
Sa syntaxe est très proche

37
00:01:16,570 --> 00:01:17,959
de l'écriture algébrique usuelle.

38
00:01:18,059 --> 00:01:20,823
Par contre, les structures de programmation originelles

39
00:01:20,923 --> 00:01:22,258
sont quasi absentes,

40
00:01:22,358 --> 00:01:23,531
une boucle à compteur

41
00:01:23,631 --> 00:01:24,718
et des sauts conditionnels

42
00:01:24,818 --> 00:01:26,564
mais aucune instruction composée algorithmique

43
00:01:26,664 --> 00:01:28,693
du type while ou if then else.

44
00:01:29,266 --> 00:01:30,609
Par contre,

45
00:01:30,709 --> 00:01:33,436
Cobol, défini en 1959-60,

46
00:01:33,536 --> 00:01:34,842
est essentiellement conçu

47
00:01:34,942 --> 00:01:36,127
pour faire des entrées/sorties.

48
00:01:36,685 --> 00:01:39,252
Ses instructions y sont riches et nombreuses

49
00:01:39,352 --> 00:01:41,275
ainsi que les capacités de décrire

50
00:01:41,375 --> 00:01:43,117
et manipuler des fichiers.

51
00:01:43,217 --> 00:01:44,676
Par contre, il ne comporte toujours

52
00:01:44,776 --> 00:01:47,644
quasi aucune instruction de calcul proprement dit.

53
00:01:48,250 --> 00:01:52,114
Et Algol, défini entre 1958 et 1960,

54
00:01:52,214 --> 00:01:54,408
a été imaginé par des algorithmiciens.

55
00:01:54,508 --> 00:01:55,912
Sa syntaxe est celle

56
00:01:56,012 --> 00:01:58,622
d'une programmation structurée mature

57
00:01:58,722 --> 00:02:00,206
mais il n'y a aucune instruction

58
00:02:00,306 --> 00:02:01,370
d'entrée/sortie standard.

59
00:02:01,470 --> 00:02:03,803
Pour cela, il a fallu attendre Algol-68,

60
00:02:03,903 --> 00:02:05,080
un autre langage

61
00:02:05,180 --> 00:02:06,727
mais un dérivé très proche d'Algol,

62
00:02:07,204 --> 00:02:09,594
qui est vraiment l'archétype presque parfait

63
00:02:09,694 --> 00:02:10,932
d'un langage algorithmique.

64
00:02:11,407 --> 00:02:12,201
Quant à Lisp,

65
00:02:12,301 --> 00:02:14,032
défini en 1958,

66
00:02:14,412 --> 00:02:16,340
il ne devait originellement servir

67
00:02:16,440 --> 00:02:17,629
qu'à décrire mathématiquement

68
00:02:17,729 --> 00:02:19,584
des programmes sous forme de formules logiques

69
00:02:19,684 --> 00:02:21,104
proches du lambda calcul.

70
00:02:21,658 --> 00:02:23,922
Bien sûr, il n'existe pas une seule façon

71
00:02:24,022 --> 00:02:25,597
de mettre de l'ordre dans cet univers

72
00:02:25,697 --> 00:02:28,211
et la répartition que nous présentons ici

73
00:02:28,311 --> 00:02:29,696
est largement arbitraire

74
00:02:30,196 --> 00:02:31,387
et bien sûr incomplète.

