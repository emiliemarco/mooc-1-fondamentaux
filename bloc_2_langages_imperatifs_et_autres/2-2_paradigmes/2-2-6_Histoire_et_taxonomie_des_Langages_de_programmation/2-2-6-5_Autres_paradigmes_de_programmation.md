## Histoire et taxonomie des Langages de programmation : Autres paradigmes de programmation

1. Premiers langages de programmation
2. Langages impératifs
3. Langages fonctionnels
4. Programmation logique
5. **Autres paradigmes de programmation**

[![Vidéo 5 B2-M2-S6 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B2-M2-S6-V5.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B2-M2-S6-V5.mp4)

## Transcription de la vidéo 

(en cours de mise en place)
