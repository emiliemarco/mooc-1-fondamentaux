1
00:00:00,656 --> 00:00:03,104
Parallèlement au renouveau des langages fonctionnels,

2
00:00:03,204 --> 00:00:05,196
est née une approche fondamentalement différente

3
00:00:05,296 --> 00:00:06,648
de programmation déclarative,

4
00:00:06,748 --> 00:00:08,374
avec la programmation logique.

5
00:00:08,688 --> 00:00:10,474
Plutôt que d'écrire une formule

6
00:00:10,574 --> 00:00:12,324
exprimant le calcul à effectuer,

7
00:00:12,424 --> 00:00:13,745
c'est-à-dire une fonction

8
00:00:13,845 --> 00:00:14,989
au sens fonctionnel,

9
00:00:15,089 --> 00:00:17,657
l'idée est de laisser encore plus de travail implicite

10
00:00:17,757 --> 00:00:19,364
à l'interpréteur ou au runtime.

11
00:00:19,645 --> 00:00:21,557
Ceci a conduit à ce qu'on a appelé

12
00:00:21,657 --> 00:00:23,183
la programmation logique.

13
00:00:23,283 --> 00:00:24,148
Dans ce contexte,

14
00:00:24,248 --> 00:00:26,172
un programme est une simple question,

15
00:00:26,272 --> 00:00:29,655
à laquelle, conformément à la logique aristotélicienne,

16
00:00:29,755 --> 00:00:32,326
on attend une réponse, oui ou non.

17
00:00:32,718 --> 00:00:35,387
On ne donne plus la formule qui y répond

18
00:00:35,487 --> 00:00:37,063
ni le raisonnement qui y conduit,

19
00:00:37,163 --> 00:00:38,839
ni évidemment la séquence d'opérations

20
00:00:38,939 --> 00:00:40,610
qui permettrait de le trouver,

21
00:00:40,710 --> 00:00:42,126
puis d'évaluer la réponse.

22
00:00:42,310 --> 00:00:43,789
Pour répondre à cette question,

23
00:00:43,889 --> 00:00:45,155
on doit fournir préalablement

24
00:00:45,255 --> 00:00:46,329
une base de connaissances

25
00:00:46,429 --> 00:00:48,104
constituée des faits connus

26
00:00:48,204 --> 00:00:49,540
et acceptés pour vrais,

27
00:00:49,640 --> 00:00:50,527
les prédicats,

28
00:00:50,627 --> 00:00:51,891
et des règles de déduction,

29
00:00:51,991 --> 00:00:53,292
de combinaison de ces prédicats

30
00:00:53,392 --> 00:00:54,927
pour établir de nouvelles vérités.

31
00:00:55,241 --> 00:00:56,826
Un tel programme définit ainsi

32
00:00:56,926 --> 00:01:00,000
ce qu'on appelle un système logique.

33
00:01:00,100 --> 00:01:02,569
Son interpréteur s'appelle un moteur d'inférence.

34
00:01:02,976 --> 00:01:05,336
Pour que ce moteur puisse être conçu,

35
00:01:05,436 --> 00:01:06,741
pour qu'il existe un algorithme 

36
00:01:06,841 --> 00:01:09,192
capable de déduire une réponse à la question

37
00:01:09,292 --> 00:01:11,000
à partir de la base de connaissances,

38
00:01:11,100 --> 00:01:13,598
il faut imposer une forme particulière aux règles permises.

39
00:01:13,766 --> 00:01:16,003
En plus, il est préférable de se limiter

40
00:01:16,103 --> 00:01:18,432
à des situations où ce moteur est efficace.

41
00:01:19,263 --> 00:01:21,237
Voilà pourquoi les langages logiques

42
00:01:21,337 --> 00:01:24,057
imposent une logique de premier ordre

43
00:01:24,157 --> 00:01:26,570
et même souvent exclusivement des clauses de Horn

44
00:01:26,670 --> 00:01:28,453
qui imposent un format particulier.

45
00:01:28,965 --> 00:01:31,559
Il existe plusieurs exemples de langages logiques.

46
00:01:31,902 --> 00:01:34,103
Mais celui qui est de loin le plus utilisé

47
00:01:34,203 --> 00:01:36,201
et qui constitue véritablement l'archétype

48
00:01:36,301 --> 00:01:37,374
de cette famille de langages,

49
00:01:37,474 --> 00:01:38,055
c'est Prolog.

50
00:01:39,148 --> 00:01:41,763
Ce langage est une création française.

51
00:01:42,731 --> 00:01:46,150
Il a été imaginé vers 1971

52
00:01:46,250 --> 00:01:48,838
par Alain Colmerauer et Philippe Roussel,

53
00:01:48,938 --> 00:01:51,296
chercheurs dans le domaine de l'intelligence artificielle

54
00:01:51,396 --> 00:01:53,359
plus précisément en traductique,

55
00:01:53,459 --> 00:01:54,986
à l'université d'Aix-Marseille.

56
00:01:55,086 --> 00:01:57,140
Pour sa définition et sa mise en œuvre,

57
00:01:57,240 --> 00:01:59,764
ils se fondaient directement sur les travaux préalables

58
00:01:59,864 --> 00:02:02,172
du logicien et informaticien Robert Kowalski,

59
00:02:02,272 --> 00:02:04,799
à l'époque, à l'université d'Édimbourg,

60
00:02:05,021 --> 00:02:07,164
sur l'interprétation procédurale

61
00:02:07,264 --> 00:02:08,854
et automatique des clauses de Horn.

62
00:02:09,110 --> 00:02:10,611
Un programme en Prolog

63
00:02:10,711 --> 00:02:13,743
est une suite de clauses, prédicats, règles ou questions.

64
00:02:13,964 --> 00:02:15,724
Pour plus d'explications sur Prolog,

65
00:02:15,824 --> 00:02:16,639
et quelques exemples,

66
00:02:16,739 --> 00:02:19,180
je vous renvoie à nouveau au support écrit.

67
00:02:20,351 --> 00:02:21,976
Dans la classe des langages déclaratifs,

68
00:02:22,076 --> 00:02:24,586
citons également la programmation par contraintes.

69
00:02:24,686 --> 00:02:27,169
On exprime le problème sous forme d'inconnues

70
00:02:27,269 --> 00:02:29,088
et on décrit les relations entre elles

71
00:02:29,188 --> 00:02:30,460
qui doivent être respectées,

72
00:02:30,560 --> 00:02:32,870
ce qui réduit l'ensemble de leurs valeurs admissibles.

73
00:02:33,735 --> 00:02:36,247
Ce sont ce qu'on appelle les contraintes.

74
00:02:36,347 --> 00:02:38,376
Un programme est un ensemble de contraintes

75
00:02:38,476 --> 00:02:40,871
qui est soumis à un compilateur ou interpréteur

76
00:02:40,971 --> 00:02:42,344
qu'on appelle un résolveur

77
00:02:42,444 --> 00:02:43,771
ou solver en anglais.

78
00:02:43,871 --> 00:02:46,500
Il existe quelques exemples de résolveurs

79
00:02:46,600 --> 00:02:47,740
généralement conçus

80
00:02:47,840 --> 00:02:49,930
pour un domaine de variables particulier.

81
00:02:50,293 --> 00:02:52,705
Pour des valeurs binaires,

82
00:02:52,805 --> 00:02:54,305
on parle de SAT-solver.

83
00:02:54,649 --> 00:02:56,639
Mais le plus souvent, il s'agit d'extensions

84
00:02:56,739 --> 00:02:58,042
ou de bibliothèques particulières

85
00:02:58,142 --> 00:02:59,275
de langages traditionnels,

86
00:02:59,375 --> 00:03:00,761
il existe de telles bibliothèques

87
00:03:00,861 --> 00:03:03,028
en Python, en C++, en Java, et cætera.

88
00:03:03,439 --> 00:03:05,333
Mais Prolog, depuis sa version 3,

89
00:03:05,433 --> 00:03:07,819
intègre réellement la programmation par contraintes

90
00:03:07,919 --> 00:03:08,964
dans le langage.

