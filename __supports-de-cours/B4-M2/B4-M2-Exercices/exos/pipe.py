#!/usr/bin/env python3

import os, sys

def main():

    # descripteurs pour lecture, écriture
    r, w = os.pipe()
    print("r = %d, w = %d"%( r, w))

    pid = os.fork()
    if pid==0:  # FILS
        os.close(w)
        rio = os.fdopen(r)
        print('Le fils lit : ', rio.read())
        rio.close()
        print('Le fils se termine')
        os._exit(0)
    else: # PERE
        os.close(r)
        wio = os.fdopen(w, 'w')
        print('Le père écrit...')
        wio.write('Bonjour fiston!')
        wio.close()
        os.waitpid(pid,0)
        print('Le père se termine')
        os._exit(0)
   
main()
