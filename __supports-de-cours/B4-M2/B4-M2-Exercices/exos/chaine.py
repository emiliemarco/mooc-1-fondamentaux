#!/usr/bin/env python3

import os

N=5

def main():
    print("proc. %d fils de %d (shell)\n" % (os.getpid(), os.getppid()))
    for i in range(N):
        newpid = os.fork()
        if newpid != 0:
            os.wait()
            break
        else:
            print("proc. %d fils de %d\n " %( os.getpid(), os.getppid()))
            continue
        
             
    os._exit(0)

main()


