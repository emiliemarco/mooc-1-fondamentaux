#!/usr/bin/env python3

import os

def main():
    i=10
    print('[père] mon pid est ', os.getpid())
    newpid = os.fork()
    if newpid == 0:
        i=i-1
        print('[fils] mon pid est ', os.getpid())
        print('[fils] mon père a comme pid ', os.getppid())
        print('[fils] la valeur de i ', i)
    else:
        i=i+1
        print('[père] mon fils a comme pid ', newpid)
        print('[père] la valeur de i ', i)
    os._exit(0)

main()
