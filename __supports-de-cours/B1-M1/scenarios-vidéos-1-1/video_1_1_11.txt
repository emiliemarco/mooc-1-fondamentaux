MOOC NSI - fondamentaux
chapitre 1
section 1
video 1_1_10
durée : 6'

Titre : Détection et correction d'erreurs

Lorsque les informations sont stockées ou transmises, il arrive
parfois que des erreurs soient introduites (par exemple, sur un réseau
non-fiable).

Il est alors utile de développer des techniques de représentation de
l'information qui permettent au récepteur de détecter la présence
d'erreurs éventuelles (auquel cas, il peut demander une
re-transmission), voire de les corriger par lui-même.

Nous allons maintenant étudier deux de ces techniques.

Elles sont toutes les deux basées sur le même principe:

introduire de la redondance dans l'information transmise, de manière à
pouvoir détecter un nombre restreint d'erreurs, voire reconstituer
l'information d'origine.


Sous-titre Bit de parité

Cette technique est sans doute la première à avoir été mise en oeuvre.

Elle consiste à ajouter, à toute information transmise, un bit (appelé
bit de parité) qui indique si le nombre de bits à 1 dans
l'information à représenter est impair (bit de parité à 1) ou pair
(bit de parité à 0).

Ainsi, si un seul bit est modifié, la parité change nécessairement, et
cette erreur peut être détectée (mais pas corrigée car on n'a pas de
moyen d'identifier le bit qui a été modifié).


De façon plus générale, on peut facilement se convaincre que la
technique du bit de parité permet de détecter un nombre
impair d'erreurs, mais ne permet d'en corriger aucune car rien
ne permet d'identifier le bit qui a été altéré.

Sous-titre Code de Hamming

Le code de Hamming est en fait une famille de codes dont la version de
base (appelée Hamming(7,4) a été introduite dans les années 1950 par
Richard Hamming

Ce système est plus puissant que le bit de parité: il permet de
corriger les erreurs de transmission, à condition que celles-ci
affectent au maximum 1 bit.

Naturellement, pour arriver à ce résultat, il faudra utiliser plus
d'un bit de contrôle.

Par exemple le codage Hammint(7,4) utilise 7 bits pour en encoder 4 et
permet de détecter et de corriger une erreur si un des 7 bits est erroné.

Le cas où c'est un bit de contrôle qui est erroné est également détecté.

La méthode précise est expliquer dans le support écrit.

Notons que les codages avec détection et correction d'erreurs sont
très utilisées par exemple dans la lecture d'audio ou de vidéos ou
encore de code QR qui fonctionne même si une partie du code est
caché.

