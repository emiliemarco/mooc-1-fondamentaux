.. MOOC_NSI documentation master file, created by
   sphinx-quickstart on Thu Feb  4 16:47:55 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to MOOC_NSI's documentation!
====================================

.. toctree::
   :maxdepth: 3
   :caption: Contenu

   Modules/glossaire_extrait.rst
	     
.. hide toctree::
    :numbered: 3
    :maxdepth: 3


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
