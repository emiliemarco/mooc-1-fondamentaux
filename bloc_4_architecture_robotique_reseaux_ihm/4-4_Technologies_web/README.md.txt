# Sommaire B4-M4

## 4-4-1 Introduction à HTML 5
- 4-4-1-1 Base et structure d'une page web : le HTML
- 4-4-1-2 HTML : éléments de base du HTML
- 4-4-1-3 HTML : mise en forme et structures avancées

## 4-4-2 Déploiement d'un site web

## 4-4-3 Interactions et gestion dynamique dans une page web

## 4-4-4 Frameworks de développement et outils de gestion de contenu
