# MOOC NSI - fondamentaux. 
## Transcription de la vidéo

## 4.3.6.2 : HTTP , FTP , SMTP

**[00:00:01]**

Pour la suite de ce chapitre 6, nous allons aborder trois protocoles de la couche 7 que sont le HTTP, le FTP et le SMTP. Un point commun de ces trois protocoles est qu'ils servent tous à transférer des données d'un hôte à l'autre. Leur différence réside notamment dans le type de données transférées et leur application. Commençons par le protocole HTTP, donc dans la vidéo précédente, nous avons vu comment un hôte pouvait résoudre un nom de domaine, ce qui sert entre autres lorsque l'on ouvre une page Web. Une fois qu'il a l'adresse IP du serveur hébergeant la page que l'on souhaite ouvrir. L'hôte désire à présent recevoir les données de cette page, à savoir le code HTML de la page ainsi que les images contenues dans cette page. Et, plus globalement, toutes les informations nécessaires au fonctionnement de cette page. Pour demander et recevoir ces données, on utilise le protocole HTTP Hypertext Transfer Protocol, ce protocole fonctionnant selon le modèle client serveur s'utilise avec diverses commandes que nous énumérerons. 
Voyons pour le moment la commande GET. La commande GET s'utilise lorsqu'un client veut obtenir le contenu hébergé sur un site. Gardons l'exemple de l'ouverture d'une page Wikipédia. Nous avons donc tapé l'adresse du site. Le serveur DNS nous a fourni l'adresse IP correspondante et on désire à présent obtenir la page en question.

**[00:01:16]**

Notre ordinateur, le client, va envoyer la requête GET suivi du chemin vers la page désirée pour la page d'accueil. À la racine, le chemin est un simple slash. Le serveur reçoit la commande GET, cherche la page au chemin demandé et, s'il la trouve, envoie la page demandée. Ensuite, le client pourra éventuellement demander l'envoi d'images contenues dans la page ou plus tard d'autres pages. La commande GET est donc une commande ne nécessitant aucun traitement côté serveur autre que l'envoi du contenu désiré. D'autres commandes, la commande POST par exemple, peuvent envoyer des données vers le serveur en vue d'être traitées par ce dernier. C'est notamment le cas lorsque l'on remplit un formulaire sur une page Web. 
Le protocole HTTP a donc pour objectif de transmettre des documents hypermédias. C'est un protocole fonctionnant selon le modèle client serveur qui utilise le protocole de transport TCP sur le port 80. Il existe de multiples requêtes. On a parlé du GET, POST. Il en existe d'autres, comme le HEAD PUT PATCH DELETE CONNECT, OPTIONS et TRACE. Certaines d'entre elles peuvent être mises en cache pour un traitement plus rapide derrière un proxy, par exemple. Il existe une variante sécurisée du protocole HTTP : HTTPS qui utilise les protocoles TLS ou SSL pour sécuriser l'envoi des données. Parlons à présent du protocole FTP File Transfert Protocol.

**[00:02:39]**

Ce protocole sert, comme son nom l'indique, à transférer des fichiers d'un serveur vers un client. Mis au point dans les années 1970, la version finale de sa spécification arrive en 1985 avec la RFC 959. Dans cette version, le protocole fonctionne dans un mode dit actif. Dans ce mode, le client à gauche demande l'envoi d'un fichier depuis le serveur. Pour cela, il envoie une requête sur le port 21 depuis un de ses ports disponibles et propose un port pour la réception des données. Ce port suit classiquement celui qui avait ouvert la connexion. Le serveur accuse réception, puis ouvre une connexion depuis son port 20 vers le port spécifié par le client. Le client accuse alors réception. Les commandes sont donc ici envoyées vers le port 21 du serveur, tandis que les données sont échangées via le port 20. 

Avec l'émergence et le déploiement des protocoles de translation d'adresse NAT et la sécurisation par les pare-feu, cette version du protocole FTP est parfois difficilement utilisable. En effet, la connexion pour l'échange de données est initiée par le serveur vers un port privé et non par le client. Ce type de comportement est refusé par défaut par la plupart des pare-feu, également sans configuration particulière. Une passerelle NAT ne pourra pas transmettre la requête au client si elle est ouverte par le serveur.

**[00:03:58]**

Ainsi, en 1998, une RFC définit un nouveau mode le mode passif. Dans ce mode, le client demande l'ouverture d'une connexion passive grâce à la commande PASV. Le serveur propose ensuite un port qu'il ouvrira pour le transfert de données. Le client pourra alors ouvrir une connexion vers ce port afin que le transfert puisse commencer. On s'affranchit ainsi des blocages évoqués puisque le client est à l'origine des connexions, commandes et données. 

Le protocole FTP est donc un protocole permettant le transfert de fichiers il se fait sur les ports 20 et 21 dans le protocole TCP. Il a deux modes : le mode actif et le mode passif que l'on vient d'évoquer. Il existe également une version sécurisée de ce protocole le FTPS, sécurisé par TLS ou SSL. 

Un mot rapide sur le protocole SMTP. Ce protocole permet l'envoi d'e mail d'un client vers un serveur pour contacter un autre utilisateur. Ce protocole relativement simple d'utilisation utilise le port 25 ou le port 587 avec authentification. Pour l'envoi d'un mail, on commence par l'adresse de l'expéditeur, puis on précise le destinataire et après validation par le serveur, on envoie alors le sujet et le corps du message. Le serveur transmet alors le message vers le serveur distant. Il existe une version sécurisée appelée ESMTP, qui est en fait une extension du protocole SMTP.



