# Exercice pratique : Sécurité des réseaux - VPN, DMZ, SSH

L’objectif de cet exercice pratique est de comprendre et mettre en œuvre les protocoles liés à la sécurité des réseaux. Cela se fera via la mise en service d’un serveur web dans une zone isolée (dans la mesure du possible) du réseau local, avec un accès via VPN/VNC et via SSH pour les modifications.

![image-32.png](./image-32.png)

Le serveur Web/SSH/VPN peut être hébergé par une carte nano-ordinateur raspberry Pi ou par un PC équipé de Linux. Il est certainement possible de faire cela avec un PC équipé de Windows également.
L'intérêt de la carte raspberry Pi est qu'elle ne nécessite pas d'écran ni de clavier, ce qui rend la solution peu chère (50 euros pour une PI3B) et peu encombrante. Ainsi, dans la suite du TP, le serveur est nommé Raspberry Pi, mais il est bien sûr possible d'utiliser un autre type de machine, d'autant plus en période de pénurie de semi-conducteurs.

L'installation du système d'exploitation sur la raspberry Pi3, pour un accès à distance (Headless dit-on communément) est décrit dans le fichier :

               
> <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/bloc_4_architecture_robotique_reseaux_ihm/4-3_reseaux/4-3-8-Exercices%20et%20TP/Installation_RaspberryPi3Bp_v3.pdf?inline=false">Installation_RaspberryPi3Bp_v3.pdf</a>


__ATTENTION__ : La carte raspberry pi étant visible de l’extérieur le temps du TP au moins, il est nécessaire que les login et mots de passe soient robustes.

Cette partie est très proche et complémentaire de l'exercice __Déploiement d'un site web__ du module 4 Technologies Web. 

## Installation d'un serveur web sur la carte Rasperry Pi, visible depuis l'extérieur.

Installer Apache sur la carte Raspberry pour en faire un serveur web.
`sudo apt install apache2`

>  * Personnaliser un minimum la page /var/www/html/index.html    
> Vérifier que le site web est visible localement :   
>  * Sur la raspberry Pi à l'adresse localhost : http://127.0.0.1  
>  * Sur une machine du réseau local en notant l'adresse IP de la carte raspberry Pi dans la barre d'adresse du navigateur.  

On veut accéder à ce serveur web depuis l’extérieur. En l’état actuel, le serveur web n'est pas visible depuis l’extérieur puisqu'il a une adresse IPv4 privée (et/ou une IPv6 publique) et que la box fait pare-feu, empêchant les requêtes venant de l'extérieur d'entrer.

## Redirection des ports extérieurs vers la carte Raspberry Pi

Une fois que l'on est bien sûr de ne plus avoir de compte _pi/raspberry_ sur la carte, configurer sur la box du domicile :

* la redirection du port 80 vers le port 80 vers la carte raspberry Pi (port forwarding) et son ouverture dans le firewall si ce n'est pas automatique.
* la redirection de tous les ports si la redirection d'un port spécifique n'est pas possible.
* Tester l’accès à distance à ce serveur web depuis un navigateur web de téléphone portable relié via le réseau cellulaire avec http://ip_publique_de_la_box.
* Configurer également la redirection du port 22 vers le port 22 de la raspberry Pi. 

Se connecter par : ssh nom_user@ip_publique_de_la_box depuis un smartphone relié via le réseau cellulaire (l'application JuiceSSH permet cela par exemple).

## Etude de la connexion SSH

l’authentification des partenaires d’une liaison sécurisée n’est pas si simple, elle nécessite le partage préalable d’un « secret » qui permet d’être certain de l’identité du correspondant (méthode utilisée par ssh) ou l’utilisation d’un mécanisme de certificats hiérarchiques qui alourdit la mise en application de cette méthode (utilisée par https).
Openssh (OpenBSD Secure Shell) est une implémentation libre d’outils de communication sécurisés basés sur le protocole SSH.
Secure Shell (SSH) est à la fois un programme informatique et un protocole de communication sécurisé. Le protocole de connexion impose un échange de clés de chiffrement en début de connexion. Par la suite, tous les segments TCP sont authentifiés et chiffrés. Il devient donc impossible d'utiliser un sniffer pour voir ce que fait l'utilisateur. (Wikipedia)

>  Exécuter Wireshark, créer une connexion SSH, puis expliquer, en s’aidant de la documentation en ligne (Wikipedia SSH et OpenSSH par exemple) les échanges conduisant à l’établissement de la liaison sécurisée. Les termes RSA, Diffie-Hellman doivent apparaître dans l’explication. 

## Mise en place d’un serveur VPN

Pour permettre de travailler à distance sur le développement de l’application serveur, on met en place un serveur VPN.
On appelle VPN (Virtual Private Network) un réseau privé virtuel chiffré qui permet d’établir un lien sécurisé entre une machine et un serveur donnant accès à un réseau distant, ou entre deux réseaux reliés par deux routeurs reliés par un lien sécurisé. Ces dispositifs permettent de traverser des réseaux non sûrs (comme internet) sans risque de compromettre les données échangées. Pour atteindre ces objectifs, le protocole utilisé doit remplir deux fonctionnalités importantes :

* authentification de l’identité des machines reliées par le VPN,
* chiffrement des communications entre ces machines.

Plusieurs solutions permettent de réaliser une connexion sécurisée, on choisit ici openvpn.

### Installation d'un serveur openvpn sur le nano-ordinateur Raspberry Pi

Le serveur OpenVPN sera la carte Raspberry Pi, le client sera un PC d'un autre sous-réseau ou un smartphone relié sur le réseau cellulaire (une application openvpn existe), 

L'installation du service openvpn peut être un peu délicate, mais il existe un script qui va beaucoup nous faciliter le travail.
Dans un terminal du PC effectuer les actions suivantes :

* `$ sudo apt update`
* `$ wget https://raw.githubusercontent.com/Angristan/openvpn-install/master/openvpn-install.sh -O debian10-vpn.sh`
* `$ chmod 755 debian10-vpn.sh`
* `$ sudo ./debian10-vpn.sh`

Le script pose un certain nombre de questions :

* adresse du service : donner l'adresse de la patte WAN de la box  
* utilisation d'ipv6 : non  
* port à utiliser : 1194  
* protocole : udp  
* DNS : réglage par défaut  
* compression : non  
* chiffrement customisé : non  
* nom du client pour lequel un fichier .ovpn va être créé  
* mot de passe : non  

C'est terminé, l’accès openvpn est prêt... ou presque. Il suffit de transférer le fichier .ovpn au client (smartphone ou PC connecté à l'extérieur du domicile)

>  * Examiner le fichier .ovpn généré. Comment est-il organisé, que contient-il ?
>
>  * Vérifier qu'il est ainsi possible de joindre le serveur VNC sur la carte raspberry Pi 3.
>
>  * Quelle est l'adresse IP allouée par le serveur au client ?
>
>  * Quelle est l'adresse IP de la raspberry Pi depuis le PC ou smartphone connecté en VNC ?
>
>  * Sur le client, comparer la table de routage avant et après la connexion.
