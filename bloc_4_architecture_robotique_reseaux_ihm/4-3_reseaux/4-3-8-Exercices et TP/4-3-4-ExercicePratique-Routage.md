# Exercice pratique : Routage

Toujours depuis le réseau du domicile, on s'intéresse au routage.

## Le NAT - Network Address Translation

### Pour le PC du domicile
Quelle est l'adresse IP apparaissant comme source des requêtes envoyées par le PC du domicile sur Internet ?
On peut pour cela utiliser le site https://ip.lafibre.info/
L'adresse IPv4 est-elle celle du PC ?
L'adresse IPv6 est-elle celle du PC ?
Expliquer ces deux réponses. Comment le lien est-il fait entre ces adresses et le PC du domicile ?

### Pour un smartphone connecté en 3/4/5G

Connecter le smartphone à Internet via le réseau cellulaire (3/4/5G). Retrouver son adresse IPv4 ( Sur Android : Paramètres > A propos du téléphone > Etat ).

Il est possible de forcer l'usage de IPv4 ou IPv6 avec le menu Paramètres > Connexions > Réseaux mobiles > Nom des points d'accès puis en cliquant sur le nom du point d'accès utilisé. On cherche alors le champ __Protocole APN__.

![image-29.png](./image-29.png)

L'adresse IPv4 utilisée par le téléphone est-elle publique ou privée ?
Quelle est l'adresse publique apparaissant sur https://ip.lafibre.info/ ?
Comment le routeur de l'opérateur télécom fait-il pour envoyer les paquets au téléphone ?

## Le routage Internet des paquets depuis le réseau local

Pour tracer le chemin d’un paquet, on utilise la commande __traceroute__ en indiquant sa destination. Les exemples sont données avec le serveur dns.google 8.8.8.8. 
L’exercice est à faire avec une autre machine publique et « lointaine », au choix.

* Windows :	`tracert dns.google` ou `tracert 8.8.8.8` par exemple
* Linux : `traceroute dns.google` ou `traceroute dns.google`

Pour faire un tracert en ipv6, on utilise :

* Windows :	tracert -6 dns.google
* Linux : 	traceroute6 dns.google

Certains routeurs ne répondent pas aux messages ICMP utilisés par traceroute, c'est pourquoi on obtient des * pour ces routeurs "muets".

### Chemin parcouru par un paquet

Faire une carte du chemin théorique suivi par le paquet. On peut pour cela utiliser le site https://ip-info.org donnant les adresses physiques des propriétaires d'une adresse IP.
Attention, il est écrit théorique car certaines IP peuvent être attribuées à une entreprise américaine et être physiquement en Europe, sur une réplication du serveur américain, par exemple.
Indiquer sur la carte les IP des routeurs traversés.

### Fonctionnement de traceroute

Lancer wireshark et faire l’acquisition des échanges liés à la requête traceroute. Le filtre __icmp__ ou __icmpv6__ est intéressant ici pour trier les paquets utiles à la question.
S’appuyer les requêtes et réponses (notamment le champ TTL) pour expliquer le fonctionnement de traceroute.

### traceroute sur le réseau cellulaire

Faire un traceroute depuis un smartphone connecté au réseau cellulaire. L'application FING permet cela notamment.
Expliquer les adresses des premiers routeurs traversés.




