# Sécurité 5/5: VLAN

1. Firewall / Proxy / DMZ
2. Chiffrement
3. Les protocoles SSH/SSL
4. VPN
5. **VLAN**

[![Vidéo 5 B4-M3-S7 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B4-M3-S7_video5_VLAN.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B4-M3-S7_video5_VLAN.mp4)

## Transcription de la vidéo 

(en cours de mise en place)
