# Introduction aux réseaux 2/5: Notions générales sur les réseaux

1. Différents réseaux pour différents usages
2. **Notions générales sur les réseaux**
3. Des transmissions essentiellement numériques
4. Introduction à Ethernet/TCP/IP
5. le modèle OSI

- Dans cette vidéo, **Anthony Juton** détaille le **vocabulaire** utilisé pour parler réseaux.

[![Vidéo 2 B4-M3-S1 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B4-M3-S1-Video2.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B4-M3-S1-Video2.mp4)

## Transcription de la vidéo 

(en cours de mise en place)
