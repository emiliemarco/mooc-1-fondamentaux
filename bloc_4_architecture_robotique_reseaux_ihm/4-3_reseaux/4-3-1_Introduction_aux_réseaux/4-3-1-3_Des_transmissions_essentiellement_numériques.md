# Introduction aux réseaux 3/5: Des transmissions essentiellement numériques

1. Différents réseaux pour différents usages
2. Notions générales sur les réseaux
3. **Des transmissions essentiellement numériques**
4. Introduction à Ethernet/TCP/IP
5. le modèle OSI

- Dans cette vidéo, **Anthony Juton** nous parle des réseaux informatiques en comparant les réseaux numériques aux réseaux analogiques qu'ils tendent à remplacer.

[![Vidéo 3 B4-M3-S1 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B4-M3-S1-Video3.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B4-M3-S1-Video3.mp4)

## Transcription de la vidéo 

(en cours de mise en place)
