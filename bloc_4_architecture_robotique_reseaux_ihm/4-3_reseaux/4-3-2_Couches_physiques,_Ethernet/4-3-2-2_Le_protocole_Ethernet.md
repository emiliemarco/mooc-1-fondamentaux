# Couches physiques, Ethernet 2/3: Le rôle de la couche physique

1. La couche physique
2. **Le protocole Ethernet**
3. Relier des hôtes : le hub et le switch

[![Vidéo 2 B4-M3-S2 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B4-M3-S2_video2_protocole_Ethernet.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B4-M3-S2_video2_protocole_Ethernet.mp4)

## Transcription de la vidéo 

(en cours de mise en place)
