# Routage 3/5: Le routage dynamique - Le protocole RIP

1. Le routage statique
2. Le protocole NAT
3. **Le routage dynamique - Le protocole RIP**
4. Le routage dynamique - Le protocole OSPF
5. Le routage dynamique - Le protocole BGP

- Dans cette vidéo, ...

[![Vidéo 3part1 B4-M3-S4 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B4-M3-S4_video3-1_RoutDyn_RIP.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B4-M3-S4_video3-1_RoutDyn_RIP.mp4)

## Transcription de la vidéo 

(en cours de mise en place)
