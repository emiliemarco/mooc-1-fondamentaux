## Complexité d'un algorithme : calcul de la complexité

1. Analyse d'algorithme
2. **Complexité d'un algorithme : calcul de la complexité**
3. Complexité d'un algorithme : ordres de grandeur
4. Preuve d'algorithme

[![Vidéo 2 part 1 B3-M1-S2 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B3-M1-S2-video1c.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B3-M1-S2-video1c.mp4)


[![Vidéo 2 part 2 B3-M1-S2 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B3-M1-S2-video1d.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B3-M1-S2-video1d.mp4)

## Présentation

[Support de la présentation des vidéos 2 à 7](https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/blob/master/__supports-de-cours/B3-M1/B3-M1-S2-part1.pdf)
