1
00:00:01,075 --> 00:00:01,906
Théorème

2
00:00:02,990 --> 00:00:05,026
On va essayer de plier le problème.

3
00:00:05,636 --> 00:00:08,311
La complexité du problème du calcul de la puissance,

4
00:00:08,411 --> 00:00:10,051
c'est grand thêta de log n.

5
00:00:12,109 --> 00:00:14,314
C'est-à-dire que vous ne pourrez pas

6
00:00:15,017 --> 00:00:16,563
faire mieux en ordre de grandeur

7
00:00:16,663 --> 00:00:17,567
que logarithme de n.

8
00:00:19,353 --> 00:00:22,270
Il existe un algorithme de coût grand O de log n,

9
00:00:22,370 --> 00:00:23,162
ça, on l'a vu,

10
00:00:23,262 --> 00:00:25,611
on sait que, au-dessus,

11
00:00:25,711 --> 00:00:27,545
on a quelque chose qui majore

12
00:00:27,852 --> 00:00:30,526
la complexité du problème.

13
00:00:32,089 --> 00:00:33,653
Et d'autre part, on va montrer

14
00:00:33,753 --> 00:00:35,218
qu'on ne peut pas faire mieux

15
00:00:36,276 --> 00:00:37,575
que logarithme en base 2 de n.

16
00:00:38,578 --> 00:00:39,689
Et ce raisonnement,

17
00:00:40,052 --> 00:00:41,391
ça, c'est un raisonnement

18
00:00:41,491 --> 00:00:42,547
qui est un petit peu en dehors

19
00:00:42,647 --> 00:00:43,825
quand même du programme de terminale,

20
00:00:43,925 --> 00:00:45,164
mais qu'il faut avoir vu,

21
00:00:45,431 --> 00:00:47,652
avoir une idée du genre de raisonnement,

22
00:00:48,433 --> 00:00:49,296
qui est le suivant.

23
00:00:49,620 --> 00:00:50,885
Je vais regarder

24
00:00:51,457 --> 00:00:52,984
le calcul de x puissance n.

25
00:00:53,684 --> 00:00:55,050
Il existe un algorithme

26
00:00:55,150 --> 00:00:57,252
qui est le plus efficace possible,

27
00:00:57,352 --> 00:00:59,485
qui ne fait que des opérations de multiplication

28
00:01:00,137 --> 00:01:01,581
pour calculer x puissance n

29
00:01:02,216 --> 00:01:03,554
et je vais essayer de voir

30
00:01:03,654 --> 00:01:04,557
que cet algorithme,

31
00:01:04,657 --> 00:01:06,121
il ne pourra pas faire mieux que log n.

32
00:01:06,484 --> 00:01:10,020
Donc, comment je m'y prends ?

33
00:01:10,120 --> 00:01:11,087
Je me dis : voilà,

34
00:01:11,187 --> 00:01:12,473
si j'ai un algorithme

35
00:01:12,793 --> 00:01:13,887
qui va le calculer,

36
00:01:14,899 --> 00:01:16,579
pour n égale 1 ou n égale 2,

37
00:01:16,884 --> 00:01:18,296
c'est clair,

38
00:01:18,396 --> 00:01:20,352
n égale 1, je n'ai pas d'opération,

39
00:01:20,646 --> 00:01:22,405
et logarithme en base 2 de 1, c'est 0.

40
00:01:22,701 --> 00:01:23,767
Pour n égale 2,

41
00:01:23,867 --> 00:01:25,096
je fais x fois x,

42
00:01:25,196 --> 00:01:26,543
je ne peux pas faire moins d'opérations

43
00:01:26,797 --> 00:01:29,384
et donc, j'ai exactement ce qu'il faut.

44
00:01:31,694 --> 00:01:34,427
Pour la suite, c'est là qu'on a un argument

45
00:01:36,936 --> 00:01:39,058
qui est un argument de nature mathématique

46
00:01:40,100 --> 00:01:42,376
mais qui est non constructible,

47
00:01:42,476 --> 00:01:43,803
non constructif on va dire.

48
00:01:45,244 --> 00:01:46,549
Je suppose que

49
00:01:46,649 --> 00:01:48,094
la propriété est vraie

50
00:01:48,194 --> 00:01:49,521
de 1 jusqu'à n - 1.

51
00:01:49,621 --> 00:01:50,472
Je fais une récurrence.

52
00:01:51,114 --> 00:01:54,185
Et je regarde un algorithme optimal

53
00:01:55,158 --> 00:01:57,225
qui calcule x puissance n.

54
00:01:57,683 --> 00:02:00,169
Alors, pourquoi il existe un algorithme optimal ?

55
00:02:00,269 --> 00:02:03,644
Parce que vous avez, en n opérations,

56
00:02:04,608 --> 00:02:08,412
en n - 1 opérations, vous pouvez faire votre calcul.

57
00:02:12,047 --> 00:02:15,329
Donc on sait qu'on est en dessous de n.

58
00:02:15,704 --> 00:02:17,410
Et puis à combiner

59
00:02:19,034 --> 00:02:20,828
les éléments à multiplier,

60
00:02:21,384 --> 00:02:23,103
le nombre de combinaisons que vous pouvez faire,

61
00:02:23,203 --> 00:02:23,916
il est fini.

62
00:02:24,400 --> 00:02:25,875
Donc il existe un plus petit.

63
00:02:26,179 --> 00:02:27,441
Donc il existe

64
00:02:27,738 --> 00:02:29,115
un algorithme optimal

65
00:02:29,215 --> 00:02:30,625
qui vous permet de résoudre le problème.

66
00:02:30,953 --> 00:02:33,981
Je ne suis pas capable de construire cet algorithme

67
00:02:34,081 --> 00:02:35,550
mais par contre je sais qu'il existe.

68
00:02:35,780 --> 00:02:37,115
Je peux raisonner dessus.

69
00:02:37,769 --> 00:02:40,003
Cet algorithme utilise

70
00:02:40,103 --> 00:02:43,801
uniquement des opérations *

71
00:02:45,577 --> 00:02:47,219
et donc maintenant,

72
00:02:48,390 --> 00:02:49,593
je vais regarder

73
00:02:49,693 --> 00:02:50,734
ce qu'il se passe.

74
00:02:51,074 --> 00:02:53,055
Alors, c'est évidemment très petit,

75
00:02:53,155 --> 00:02:54,464
vous pourrez récupérer les choses,

76
00:02:54,564 --> 00:02:55,498
je vous donne juste l'idée.

77
00:02:57,246 --> 00:02:59,323
Lorsque j'exécute mon algorithme,

78
00:03:00,803 --> 00:03:03,942
je vais faire une dernière opération de multiplication

79
00:03:04,256 --> 00:03:06,205
dans cet algorithme que j'exécute.

80
00:03:06,549 --> 00:03:08,896
Cette dernière opération de multiplication

81
00:03:08,996 --> 00:03:10,371
va me donner x puissance n.

82
00:03:11,101 --> 00:03:13,598
Donc elle est obligatoirement de la forme

83
00:03:13,698 --> 00:03:15,866
x puissance k fois x puissance n - k

84
00:03:15,966 --> 00:03:16,828
pour avoir x puissance n

85
00:03:16,928 --> 00:03:18,496
puisque ce sont les seules possibilités que j'ai.

86
00:03:19,188 --> 00:03:21,069
Donc je vais avoir à calculer

87
00:03:21,169 --> 00:03:22,394
un x puissance k

88
00:03:22,670 --> 00:03:24,157
et un x puissance n - k.

89
00:03:25,027 --> 00:03:26,419
En fait, ce que je vais regarder,

90
00:03:26,519 --> 00:03:28,450
c'est la valeur entre k et n - k

91
00:03:28,550 --> 00:03:29,388
qui est la plus grande,

92
00:03:29,689 --> 00:03:31,964
et comme la somme des deux fait n,

93
00:03:32,247 --> 00:03:34,185
l'un des deux termes est au moins

94
00:03:34,285 --> 00:03:35,582
égal à n / 2.

95
00:03:36,623 --> 00:03:39,488
Donc si c'est supérieur ou égal à n / 2,

96
00:03:39,588 --> 00:03:40,283
ce que j'ai, c'est que

97
00:03:41,492 --> 00:03:43,181
le coût pour calculer

98
00:03:44,384 --> 00:03:45,439
x puissance k

99
00:03:45,771 --> 00:03:47,757
va être supérieur au coût

100
00:03:47,857 --> 00:03:50,199
pour calculer n / 2

101
00:03:50,507 --> 00:03:51,757
et va donc demander

102
00:03:51,857 --> 00:03:53,892
au moins logarithme en base 2 de k

103
00:03:53,992 --> 00:03:54,855
opérations

104
00:03:54,955 --> 00:03:55,938
c'est-à-dire au moins

105
00:03:56,038 --> 00:03:58,627
logarithme en base 2 de n / 2

106
00:03:58,925 --> 00:04:00,505
qui est logarithme en base 2 de n

107
00:04:00,605 --> 00:04:01,328
moins 1.

108
00:04:01,908 --> 00:04:04,681
Or, à la fin, je fais une dernière opération

109
00:04:05,327 --> 00:04:07,092
qui est l'opération ici,

110
00:04:07,693 --> 00:04:09,223
et cette opération ici

111
00:04:09,323 --> 00:04:10,370
correspond au 1

112
00:04:11,071 --> 00:04:12,154
que vous avez là.

113
00:04:13,067 --> 00:04:13,948
Ça,

114
00:04:15,074 --> 00:04:18,490
logarithme en base 2 de n moins 1,

115
00:04:19,388 --> 00:04:20,881
c'est un minorant

116
00:04:20,981 --> 00:04:22,611
du calcul de x puissance k,

117
00:04:22,711 --> 00:04:24,983
le 1, c'est ce que je suis obligé de faire

118
00:04:25,693 --> 00:04:27,888
pour faire la dernière multiplication.

119
00:04:28,562 --> 00:04:30,005
Et quand vous regardez la somme des deux,

120
00:04:30,105 --> 00:04:31,898
vous obtenez logarithme en base 2 de n.

121
00:04:33,163 --> 00:04:35,335
Alors, quelle est la moralité de l'histoire ?

122
00:04:36,361 --> 00:04:37,497
Je ne l'ai pas écrite,

123
00:04:37,597 --> 00:04:38,939
je vais juste vous la donner à l'oral.

124
00:04:39,284 --> 00:04:40,488
La moralité de l'histoire,

125
00:04:40,588 --> 00:04:42,920
c'est que vous ne pourrez pas trouver

126
00:04:43,020 --> 00:04:44,858
d'algorithme qui fasse mieux

127
00:04:44,958 --> 00:04:46,625
que logarithme en base 2 de n.

128
00:04:48,088 --> 00:04:49,536
Vous avez déjà

129
00:04:49,636 --> 00:04:51,381
un algorithme qui fait du

130
00:04:51,481 --> 00:04:52,740
logarithme en base 2 de n,

131
00:04:53,099 --> 00:04:54,057
c'est inférieur ou égal à

132
00:04:54,157 --> 00:04:56,630
2 logarithme en base 2 de n.

133
00:04:56,962 --> 00:04:58,710
Donc si vous trouvez un algorithme

134
00:04:58,810 --> 00:05:00,537
meilleur que l'algorithme standard,

135
00:05:00,637 --> 00:05:01,929
il va se situer entre les deux,

136
00:05:02,429 --> 00:05:05,017
entre log n et 2 log n.

137
00:05:06,043 --> 00:05:07,125
Donc en fait,

138
00:05:08,195 --> 00:05:09,733
il n'y a pas beaucoup de place.

139
00:05:09,833 --> 00:05:11,672
C'est-à-dire que l'algorithme standard

140
00:05:11,772 --> 00:05:13,629
va être particulièrement efficace.

141
00:05:13,964 --> 00:05:16,324
Vous êtes dans le bon ordre de grandeur.

142
00:05:16,683 --> 00:05:19,096
Et donc cette exponentiation rapide,

143
00:05:19,196 --> 00:05:20,921
parce que c'est comme ça qu'on appelle cet algorithme,

144
00:05:21,152 --> 00:05:23,035
va être efficace,

145
00:05:23,135 --> 00:05:24,618
et même optimal

146
00:05:24,718 --> 00:05:25,981
au sens de l'efficacité,

147
00:05:26,081 --> 00:05:27,666
c'est-à-dire qu'il n'existe pas d'algorithme

148
00:05:27,766 --> 00:05:29,651
de complexité plus faible

149
00:05:29,999 --> 00:05:31,039
de celui que vous avez là.

150
00:05:32,501 --> 00:05:34,285
Voilà, on arrive

151
00:05:35,295 --> 00:05:36,785
à la phase finale

152
00:05:36,885 --> 00:05:38,348
de l'analyse de complexité,

153
00:05:38,648 --> 00:05:39,795
sur la complexité

154
00:05:40,581 --> 00:05:41,506
de problèmes.

155
00:05:42,099 --> 00:05:44,783
Donc, étant donné un problème P,

156
00:05:44,883 --> 00:05:46,881
on appelle complexité du problème P

157
00:05:47,235 --> 00:05:50,603
le minimum

158
00:05:50,703 --> 00:05:52,170
sur tous les algorithmes

159
00:05:52,424 --> 00:05:53,947
qui résolvent le problème P

160
00:05:54,179 --> 00:05:57,214
de la complexité de l'algorithme

161
00:05:57,314 --> 00:05:58,167
en fonction de n.

162
00:05:58,642 --> 00:06:00,734
Je ne peux pas descendre en dessous de ça.

163
00:06:01,358 --> 00:06:02,851
Et ça, c'est vraiment important.

164
00:06:03,098 --> 00:06:04,184
C'est-à-dire ça veut dire que

165
00:06:04,433 --> 00:06:06,423
en gros, vous aurez beau faire,

166
00:06:06,523 --> 00:06:08,226
vous n'arriverez pas à réduire

167
00:06:08,326 --> 00:06:09,771
les calculs plus que ça.

168
00:06:10,711 --> 00:06:12,919
Alors on appelle en général Ca de n

169
00:06:13,019 --> 00:06:14,051
la complexité au pire

170
00:06:14,151 --> 00:06:15,785
et donc la complexité du problème,

171
00:06:16,019 --> 00:06:18,582
c'est surtout vu comme une complexité au pire.

172
00:06:19,550 --> 00:06:20,606
Alors, qu'est-ce qu'on peut dire ?

173
00:06:20,850 --> 00:06:22,739
La complexité d'un algorithme évidemment

174
00:06:22,839 --> 00:06:24,037
si vous avez un algorithme,

175
00:06:24,985 --> 00:06:27,645
sa complexité est supérieure

176
00:06:28,183 --> 00:06:29,566
à la complexité du problème.

177
00:06:30,693 --> 00:06:32,096
Par contre, ce qui est complexe,

178
00:06:32,196 --> 00:06:34,992
c'est que la complexité d'un problème,

179
00:06:35,092 --> 00:06:36,494
c'est très difficile à avoir.

180
00:06:36,810 --> 00:06:37,948
Il y a des problèmes pour lesquels

181
00:06:38,048 --> 00:06:39,355
on ne sait toujours pas

182
00:06:39,973 --> 00:06:42,167
comment calculer la complexité.

183
00:06:43,246 --> 00:06:44,075
On a des algos

184
00:06:44,340 --> 00:06:45,941
mais la valeur de

185
00:06:46,216 --> 00:06:47,872
la complexité du problème est inconnue.

186
00:06:48,110 --> 00:06:50,159
C'est l'exemple, par exemple, du produit de matrices.

187
00:06:51,869 --> 00:06:54,778
Ensuite, le coût d'un algorithme sur une donnée,

188
00:06:54,878 --> 00:06:56,338
ça ne vous donne aucune information

189
00:06:56,438 --> 00:06:59,354
sur la complexité d'un problème.

190
00:07:00,007 --> 00:07:01,581
Vous avez juste une valeur

191
00:07:01,681 --> 00:07:02,957
et ça ne vous dit pas plus.

192
00:07:03,713 --> 00:07:06,411
Et puis la référence à la machine est importante,

193
00:07:06,511 --> 00:07:08,432
c'est quoi, les opérateurs que l'on compte ?

194
00:07:08,625 --> 00:07:10,417
Et la complexité que vous allez regarder,

195
00:07:10,517 --> 00:07:11,565
la complexité d'un problème

196
00:07:11,665 --> 00:07:13,527
est toujours en fonction des opérations

197
00:07:13,627 --> 00:07:14,886
que vous avez à votre disposition.

198
00:07:15,873 --> 00:07:17,612
Alors, on peut aller un peu plus loin

199
00:07:17,712 --> 00:07:21,676
en disant on va se référer à une machine universelle

200
00:07:22,404 --> 00:07:23,746
qui est la machine de Turing,

201
00:07:23,846 --> 00:07:26,219
et on va se mettre dans un modèle de complexité

202
00:07:26,319 --> 00:07:27,309
sur la machine de Turing.

203
00:07:27,538 --> 00:07:29,638
Ça, c'est une des solutions que l'on peut avoir.

204
00:07:31,093 --> 00:07:32,993
Alors, continuons un petit peu.

205
00:07:34,198 --> 00:07:36,588
Si vous recherchez un élément dans un tableau de taille n,

206
00:07:37,215 --> 00:07:38,995
vous allez parcourir l'ensemble

207
00:07:39,095 --> 00:07:40,695
et donc vous avez quelque chose en grand O de n.

208
00:07:40,957 --> 00:07:42,170
Vous ne pourrez pas

209
00:07:42,823 --> 00:07:44,607
faire mieux que grand O de n.

210
00:07:44,876 --> 00:07:46,742
Par contre, si c'est ordonné,

211
00:07:48,742 --> 00:07:50,043
vous ne pourrez pas faire mieux

212
00:07:50,143 --> 00:07:51,704
que logarithme en base 2 de n.

213
00:07:52,473 --> 00:07:53,782
C'est la recherche dichotomique.

214
00:07:54,069 --> 00:07:54,969
Alors, comment est-ce qu'on montre

215
00:07:55,069 --> 00:07:56,072
qu'on ne peut pas faire mieux ?

216
00:07:57,783 --> 00:07:59,474
Alors, j'ai un argument

217
00:07:59,574 --> 00:08:01,713
qui est un argument de type théorie de l'information

218
00:08:02,025 --> 00:08:03,443
qui est très simple.

219
00:08:04,322 --> 00:08:06,616
Vous avez à rechercher un élément

220
00:08:06,716 --> 00:08:08,957
dont l'indice est entre 1 et n.

221
00:08:09,477 --> 00:08:11,407
Donc vous avez à déterminer l'indice.

222
00:08:12,872 --> 00:08:15,836
À chaque fois que vous faites une opération de comparaison,

223
00:08:17,522 --> 00:08:19,459
plus petit, plus grand ou égal,

224
00:08:21,513 --> 00:08:23,925
ça vous donne en gros un bit d'information.

225
00:08:25,764 --> 00:08:28,825
Or, pour coder un entier de 1 à n,

226
00:08:28,925 --> 00:08:31,441
il vous faut au minimum

227
00:08:31,541 --> 00:08:33,770
logarithme en base 2 de n bits.

228
00:08:34,495 --> 00:08:36,035
Donc en fait, vous ne pourrez pas faire mieux

229
00:08:36,909 --> 00:08:38,495
puisqu'à chaque fois, vous avez une information

230
00:08:38,595 --> 00:08:41,516
qui vous donne un bit d'information,

231
00:08:41,641 --> 00:08:42,690
vous ne pourrez pas faire mieux

232
00:08:42,790 --> 00:08:45,099
que logarithme en base 2 de n.

233
00:08:45,741 --> 00:08:48,311
Donc, pour les multiplications,

234
00:08:48,598 --> 00:08:50,500
c'est le même raisonnement et on l'a vu.

235
00:08:50,750 --> 00:08:52,092
Et pour le tri d'un tableau,

236
00:08:52,192 --> 00:08:53,362
on sait qu'on a des algorithmes

237
00:08:53,462 --> 00:08:54,554
qui font du n log n

238
00:08:55,463 --> 00:08:57,212
donc on sait que la complexité du problème

239
00:08:57,312 --> 00:08:59,204
est, en comparaison d'éléments,

240
00:08:59,304 --> 00:09:02,698
au plus du n log n.

241
00:09:02,798 --> 00:09:03,963
Est-ce qu'on peut faire mieux ?

242
00:09:04,920 --> 00:09:06,150
Ce n'est pas évident.

243
00:09:06,419 --> 00:09:08,141
Alors je donne là encore

244
00:09:08,241 --> 00:09:09,856
un exemple qui va vous dire

245
00:09:14,082 --> 00:09:16,319
que le tri est un problème

246
00:09:16,419 --> 00:09:18,078
de complexité grand O de n log n

247
00:09:18,178 --> 00:09:19,359
en nombre de comparaisons,

248
00:09:19,968 --> 00:09:22,684
et c'est encore un argument de codage.

249
00:09:23,143 --> 00:09:25,294
À chaque fois que vous faites une comparaison

250
00:09:26,545 --> 00:09:27,553
entre deux éléments,

251
00:09:27,653 --> 00:09:29,199
vous avez un bit d'information

252
00:09:30,139 --> 00:09:31,160
et faire un tri,

253
00:09:31,421 --> 00:09:32,659
ça revient

254
00:09:33,045 --> 00:09:35,841
à identifier une permutation des éléments

255
00:09:35,941 --> 00:09:37,416
parmi toutes les permutations.

256
00:09:38,815 --> 00:09:41,945
Donc un tri de n éléments,

257
00:09:42,212 --> 00:09:44,408
ça revient à construire,

258
00:09:47,135 --> 00:09:48,869
à choisir une permutation

259
00:09:48,969 --> 00:09:51,048
parmi les factorielle n permutations possibles,

260
00:09:51,531 --> 00:09:53,039
et donc, pour avoir ça,

261
00:09:53,139 --> 00:09:54,986
vous avez une réponse de 0 et de 1 à chaque fois,

262
00:09:55,086 --> 00:09:56,075
c'est la comparaison,

263
00:09:56,364 --> 00:09:57,804
et bien vous avez

264
00:09:58,081 --> 00:09:59,902
un bit d'information à chaque fois

265
00:10:00,002 --> 00:10:03,997
et donc, pour coder une permutation,

266
00:10:04,097 --> 00:10:05,355
vous avez une séquence de bits.

267
00:10:06,010 --> 00:10:07,110
Or, pour distinguer

268
00:10:07,210 --> 00:10:09,420
toutes les permutations les unes des autres,

269
00:10:09,520 --> 00:10:11,810
il faut voir sur combien de bits vous allez coder

270
00:10:12,029 --> 00:10:13,914
les permutations.

271
00:10:14,268 --> 00:10:16,089
J'ai factorielle n permutations

272
00:10:16,847 --> 00:10:19,052
et bien, pour coder toutes ces permutations

273
00:10:19,152 --> 00:10:20,359
sur un ensemble de bits,

274
00:10:20,459 --> 00:10:23,284
il me faut logarithme en base 2 de factorielle n.

275
00:10:23,621 --> 00:10:25,617
Ça, ça me permet de distinguer toutes les permutations.

276
00:10:26,282 --> 00:10:28,599
Donc en gros, si je veux identifier une permutation,

277
00:10:28,699 --> 00:10:31,030
je dois disposer de logarithme en base 2

278
00:10:31,431 --> 00:10:33,692
de factorielle n bits.

279
00:10:34,298 --> 00:10:35,823
Donc la complexité

280
00:10:35,983 --> 00:10:37,392
du problème de tri,

281
00:10:37,704 --> 00:10:39,643
c'est du grand O de

282
00:10:39,880 --> 00:10:41,068
logarithme en base 2

283
00:10:41,168 --> 00:10:42,254
de factorielle n,

284
00:10:42,860 --> 00:10:44,500
ce qui correspond à du n log n.

285
00:10:45,181 --> 00:10:47,367
Donc le tri, vous ne pourrez jamais

286
00:10:47,467 --> 00:10:48,599
descendre en dessous,

287
00:10:48,699 --> 00:10:50,429
si vous prenez comme opérateur

288
00:10:50,706 --> 00:10:52,084
l'opérateur de comparaison,

289
00:10:52,331 --> 00:10:53,556
qui va vous donner

290
00:10:53,832 --> 00:10:56,580
le nombre minimum d'opérations à faire.

291
00:10:58,772 --> 00:11:01,119
Alors, si je reprends le message

292
00:11:01,688 --> 00:11:03,274
global de cette partie,

293
00:11:03,815 --> 00:11:06,381
vous avez un problème, des données,

294
00:11:06,481 --> 00:11:07,796
un algorithme, une machine

295
00:11:10,187 --> 00:11:13,332
et vous obtenez un résultat,

296
00:11:13,432 --> 00:11:14,587
alors je n'ai pas remis

297
00:11:14,687 --> 00:11:17,360
le texte du programme, et cætera.

298
00:11:17,638 --> 00:11:19,299
On cherche à intuiter

299
00:11:19,399 --> 00:11:20,871
ou à avoir une approximation

300
00:11:20,971 --> 00:11:23,053
de la ressource consommée sur la machine,

301
00:11:23,571 --> 00:11:26,127
et donc on va avoir un certain nombre de choses

302
00:11:26,227 --> 00:11:27,049
qui nous seront données.

303
00:11:27,365 --> 00:11:29,410
Il y a l'information sur les données au départ,

304
00:11:29,684 --> 00:11:32,370
il y a la complexité qui est liée à l'algorithme,

305
00:11:32,470 --> 00:11:34,882
la façon dont vous avez utilisé la machine

306
00:11:34,982 --> 00:11:36,400
et vous avez un lien entre les trois.

307
00:11:37,656 --> 00:11:39,582
Et donc, quand on va parler

308
00:11:41,576 --> 00:11:42,977
d'analyse de complexité,

309
00:11:43,077 --> 00:11:44,892
on va avoir plusieurs points.

310
00:11:45,922 --> 00:11:48,278
Le premier point qui intéresse effectivement

311
00:11:48,759 --> 00:11:50,399
la personne qui va coder,

312
00:11:50,646 --> 00:11:52,745
c'est in fine le résultat final.

313
00:11:52,971 --> 00:11:54,159
C'est la performance

314
00:11:54,471 --> 00:11:57,238
de votre implémentation.

315
00:11:58,261 --> 00:12:00,858
Ici. C'est-à-dire c'est

316
00:12:00,958 --> 00:12:02,429
l'utilisation des ressources.

317
00:12:02,691 --> 00:12:03,981
Qu'est-ce que vous avez utilisé

318
00:12:04,081 --> 00:12:05,100
pour résoudre votre problème ?

319
00:12:05,821 --> 00:12:07,435
Ensuite, vous avez

320
00:12:07,535 --> 00:12:08,929
le nombre d'opérations,

321
00:12:09,029 --> 00:12:10,114
le coût de l'algorithme.

322
00:12:11,903 --> 00:12:14,254
Vous avez également le coût du programme

323
00:12:14,545 --> 00:12:16,872
si vous regardez de façon intermédiaire.

324
00:12:17,258 --> 00:12:19,166
Le coût de l'algorithme, c'est le nombre d'opérations.

325
00:12:20,118 --> 00:12:21,536
Quand vous remontez un peu,

326
00:12:21,636 --> 00:12:24,506
vous avez la complexité de l'algorithme.

327
00:12:24,606 --> 00:12:26,230
En gros, c'est un majorant sur

328
00:12:26,480 --> 00:12:29,002
la pire des données que vous puissiez avoir.

329
00:12:29,571 --> 00:12:30,846
Et tout en haut,

330
00:12:30,946 --> 00:12:32,787
vous avez la complexité du problème.

331
00:12:32,887 --> 00:12:34,071
C'est-à-dire, si on vous demande

332
00:12:34,171 --> 00:12:35,809
de faire une opération de tri,

333
00:12:36,253 --> 00:12:37,201
vous savez

334
00:12:37,301 --> 00:12:38,926
qu'en nombre de comparaisons d'éléments,

335
00:12:39,176 --> 00:12:40,807
vous ne pourrez pas faire mieux que ça.

336
00:12:41,075 --> 00:12:43,583
Vous avez la notion de complexité de problème

337
00:12:43,683 --> 00:12:45,197
qui est un peu au dessus

338
00:12:45,493 --> 00:12:47,072
et qui vous guide

339
00:12:47,172 --> 00:12:48,361
dans la façon que vous avez

340
00:12:48,461 --> 00:12:49,868
de résoudre votre problème.

341
00:12:54,340 --> 00:12:55,776
Donc on a un modèle,

342
00:12:57,377 --> 00:12:58,419
le voilà,

343
00:12:58,763 --> 00:13:00,517
un modèle de production d'information

344
00:13:00,617 --> 00:13:01,980
en fonction des ressources utilisées

345
00:13:02,080 --> 00:13:02,921
c'est-à-dire que

346
00:13:03,707 --> 00:13:07,516
vous allez revoir votre algorithme

347
00:13:07,726 --> 00:13:10,088
à travers le prisme de l'utilisation des ressources.

