## Complexité d'un algorithme : ordres de grandeur

1. Analyse d'algorithme
2. Complexité d'un algorithme : calcul de la complexité
3. **Complexité d'un algorithme : ordres de grandeur**
4. Preuve d'algorithme

[![Vidéo 3 part 1 B3-M1-S2 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B3-M1-S2-video1e.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B3-M1-S2-video1e.mp4)


[![Vidéo 3 part 2 B3-M1-S2 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B3-M1-S2-video1f.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B3-M1-S2-video1f.mp4)


[![Vidéo 3 part 3 B3-M1-S2 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B3-M1-S2-video1g.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B3-M1-S2-video1g.mp4)

## Présentation

[Support de la présentation des vidéos 2 à 7](https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/blob/master/__supports-de-cours/B3-M1/B3-M1-S2-part1.pdf)
