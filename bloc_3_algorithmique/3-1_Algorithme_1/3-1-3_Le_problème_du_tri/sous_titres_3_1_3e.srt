1
00:00:02,229 --> 00:00:03,485
Une fois qu'on a fait ça,

2
00:00:03,585 --> 00:00:05,705
on peut analyser le coût de cet algorithme.

3
00:00:07,894 --> 00:00:10,254
Il faut que je me donne une idée de la taille

4
00:00:10,354 --> 00:00:11,778
et les opérations de référence.

5
00:00:12,250 --> 00:00:14,498
L'opération de référence que je vais prendre ici,

6
00:00:14,598 --> 00:00:15,749
c'est la comparaison d'éléments

7
00:00:15,849 --> 00:00:17,595
puisque c'est ça qui va compter

8
00:00:18,176 --> 00:00:21,414
du point de vue, je dirais, intérêt de l'algorithme,

9
00:00:21,514 --> 00:00:23,642
c'est la chose qui est spécifique aux éléments.

10
00:00:25,005 --> 00:00:29,045
Et puis, comme je suis sur des données,

11
00:00:29,145 --> 00:00:30,827
je m'intéresse à la taille des données,

12
00:00:30,927 --> 00:00:34,144
ici, j'ai un tableau qu'on va prendre de taille n,

13
00:00:34,244 --> 00:00:36,600
et on a vu qu'on a un arbre qui est associé,

14
00:00:36,700 --> 00:00:37,544
un arbre de décision,

15
00:00:37,927 --> 00:00:40,889
qui va être de taille quelque chose.

16
00:00:41,227 --> 00:00:42,991
Si je regarde mon arbre de décision,

17
00:00:43,091 --> 00:00:44,967
c'est un arbre qui est tout à fait simple,

18
00:00:45,067 --> 00:00:47,766
il a un nœud au premier étage,

19
00:00:47,866 --> 00:00:49,527
deux nœuds au deuxième étage,

20
00:00:49,627 --> 00:00:51,108
et puis quatre, et puis huit, et cætera.

21
00:00:51,464 --> 00:00:55,140
Et donc il peut avoir jusqu'à 2 puissance (k + 1) - 1

22
00:00:55,240 --> 00:00:56,826
sur le dernier étage.

23
00:00:57,136 --> 00:00:58,765
Alors là, ça va régler d'un seul coup

24
00:00:58,865 --> 00:01:00,241
tous les problèmes qui sont liés

25
00:01:00,638 --> 00:01:05,643
au fait que votre tableau n'est pas forcément

26
00:01:06,097 --> 00:01:08,834
de taille égale à 2 puissance (k + 1) - 1

27
00:01:09,141 --> 00:01:11,227
mais on se rend compte que si

28
00:01:13,213 --> 00:01:14,618
je prends des tableaux de cette taille,

29
00:01:14,718 --> 00:01:16,112
je vais avoir effectivement

30
00:01:16,212 --> 00:01:17,703
tous les cas possibles

31
00:01:17,937 --> 00:01:19,871
et donc, ce qui va compter,

32
00:01:19,971 --> 00:01:21,987
c'est le nombre de décisions que je vais prendre

33
00:01:22,207 --> 00:01:23,604
et le nombre de décisions que je vais prendre,

34
00:01:23,704 --> 00:01:25,244
ça correspond à la hauteur de l'arbre

35
00:01:25,344 --> 00:01:25,933
plus ou moins 1,

36
00:01:26,033 --> 00:01:28,283
c'est la partie entière supérieure qu'il faudrait regarder.

37
00:01:29,500 --> 00:01:32,177
Donc, en faisant ce calcul-là,

38
00:01:32,277 --> 00:01:33,212
je me rends compte que

39
00:01:34,290 --> 00:01:35,099
ce qui va compter,

40
00:01:35,199 --> 00:01:36,826
c'est la hauteur de l'arbre

41
00:01:36,926 --> 00:01:37,632
et la hauteur de l'arbre

42
00:01:37,732 --> 00:01:39,850
elle est de l'ordre de logarithme de n

43
00:01:40,267 --> 00:01:44,200
puisque j'ai n qui est égal à ceci.

44
00:01:44,999 --> 00:01:47,113
Donc on a quelque chose qui est de l'ordre

45
00:01:47,213 --> 00:01:48,929
de logarithme en base 2 de n

46
00:01:49,029 --> 00:01:50,873
ce qui correspond effectivement

47
00:01:50,973 --> 00:01:52,671
à quelque chose de particulièrement efficace.

48
00:01:53,021 --> 00:01:55,100
On a des exemples aux limites.

49
00:01:55,789 --> 00:01:57,799
Le coût au mieux sur la version non simplifiée,

50
00:01:57,899 --> 00:02:01,155
il suffit que l'élément recherché soit celui du milieu,

51
00:02:01,255 --> 00:02:04,437
et là, ça va aller très vite.

52
00:02:05,561 --> 00:02:07,567
Alors là, c'est la version non simplifiée,

53
00:02:07,667 --> 00:02:10,091
sinon, si je prends la version simplifiée,

54
00:02:10,191 --> 00:02:12,469
effectivement je vais descendre systématiquement jusqu'en bas.

55
00:02:13,126 --> 00:02:14,962
Si je regarde le coût au pire,

56
00:02:15,193 --> 00:02:17,046
pour accéder au premier élément,

57
00:02:17,146 --> 00:02:20,682
il me faut de l'ordre de logarithme en base 2 de n opérations.

58
00:02:21,650 --> 00:02:23,450
Si c'est le premier,

59
00:02:23,550 --> 00:02:25,845
je suis obligé de descendre et de faire toutes les décisions,

60
00:02:25,945 --> 00:02:27,630
de balayer tout l'arbre.

61
00:02:28,266 --> 00:02:31,104
Effectivement, j'ai quelque chose qui est en logarithme de n

62
00:02:31,204 --> 00:02:33,441
donc c'est quelque chose de très rapide

63
00:02:33,904 --> 00:02:36,536
par rapport à ce que l'on avait vu jusqu'à présent.

64
00:02:37,723 --> 00:02:45,053
Alors, si je reprends mon algorithme,

65
00:02:45,153 --> 00:02:46,662
en fait,  là, je suis quand même

66
00:02:46,762 --> 00:02:49,218
beaucoup, beaucoup, beaucoup plus efficace

67
00:02:49,707 --> 00:02:50,940
pour rechercher un élément

68
00:02:51,040 --> 00:02:53,334
que l'algorithme qui parcourait toute la structure.

69
00:02:53,710 --> 00:02:57,741
Ici, j'ai quelque chose qui va aller en log n

70
00:02:57,841 --> 00:02:59,119
donc ça va être très rapide.

71
00:02:59,219 --> 00:03:00,784
Alors, il faut bien comprendre ce que c'est que log n.

72
00:03:01,388 --> 00:03:03,409
Si vous avez à rechercher un élément

73
00:03:03,509 --> 00:03:05,335
parmi mille éléments dans un tableau trié,

74
00:03:05,657 --> 00:03:06,875
c'est de l'ordre de 10.

75
00:03:06,975 --> 00:03:08,612
Si vous avez à rechercher

76
00:03:10,731 --> 00:03:13,078
un élément dans un tableau trié,

77
00:03:13,528 --> 00:03:15,729
de taille mille,

78
00:03:15,829 --> 00:03:17,719
ça va vous faire de l'ordre de 10 itérations.

79
00:03:18,094 --> 00:03:20,190
Si vous avez à rechercher un élément

80
00:03:20,290 --> 00:03:22,605
dans un tableau trié de taille un million,

81
00:03:22,705 --> 00:03:23,918
ce qui n'est pas déraisonnable,

82
00:03:24,018 --> 00:03:26,995
pensez par exemple à des requêtes http

83
00:03:27,095 --> 00:03:29,794
qui sont triées et sur lesquelles

84
00:03:29,894 --> 00:03:31,311
vous pouvez essayer de retrouver un élément,

85
00:03:31,411 --> 00:03:33,546
un million, ce n'est pas grand chose.

86
00:03:33,883 --> 00:03:35,725
Pensez aux bases de données génoniques.

87
00:03:36,853 --> 00:03:39,961
Si vous avez un million d'éléments,

88
00:03:40,061 --> 00:03:41,574
rechercher de façon exhaustive,

89
00:03:41,674 --> 00:03:43,564
ça va vous prendre un million d'itérations,

90
00:03:43,664 --> 00:03:44,721
un million de comparaisons,

91
00:03:45,093 --> 00:03:46,722
si vous avez trié au préalable,

92
00:03:46,822 --> 00:03:47,896
ça vous prend quelque chose

93
00:03:47,996 --> 00:03:49,781
qui est de l'ordre de 20 itérations.

94
00:03:50,157 --> 00:03:51,473
Donc vous voyez que c'est vraiment

95
00:03:51,573 --> 00:03:52,852
quelque chose de drastique,

96
00:03:52,952 --> 00:03:55,071
ça réduit très fortement

97
00:03:57,375 --> 00:03:58,490
la complexité.

98
00:04:00,966 --> 00:04:02,358
Prenons un peu de distance

99
00:04:02,458 --> 00:04:03,884
par rapport à ce qu'on vient de voir.

100
00:04:03,984 --> 00:04:05,409
Si je cherche un élément,

101
00:04:05,509 --> 00:04:07,965
je vais avoir un premier aspect.

102
00:04:08,333 --> 00:04:10,129
C'est si les éléments sont comparables,

103
00:04:10,229 --> 00:04:11,123
je vais les trier

104
00:04:11,223 --> 00:04:12,702
et donc je vais pouvoir

105
00:04:13,476 --> 00:04:14,993
les rechercher par un algorithme

106
00:04:15,093 --> 00:04:15,984
de type dichotomie,

107
00:04:16,776 --> 00:04:18,836
qui est, on verra ça plus tard,

108
00:04:18,936 --> 00:04:20,057
du diviser pour régner,

109
00:04:20,518 --> 00:04:22,195
et surtout ce qui est intéressant,

110
00:04:22,295 --> 00:04:24,594
c'est que le fait de trier les éléments

111
00:04:24,694 --> 00:04:25,983
c'est une sorte de pré-calcul

112
00:04:26,083 --> 00:04:26,696
qui fait que,

113
00:04:27,050 --> 00:04:29,418
quand je vais avoir besoin de rechercher un autre élément,

114
00:04:29,797 --> 00:04:30,574
il n'y a pas de souci,

115
00:04:30,674 --> 00:04:32,455
je vais retrouver cet autre élément.

116
00:04:32,800 --> 00:04:34,108
C'est ça qui est efficace.

117
00:04:34,550 --> 00:04:36,603
La complexité du pré-calcul,

118
00:04:36,991 --> 00:04:38,521
au mieux, ça va être du n log n,

119
00:04:38,621 --> 00:04:40,294
on verra ça avec les algorithmes

120
00:04:40,394 --> 00:04:41,616
de type diviser pour régner.

121
00:04:41,926 --> 00:04:43,890
Et la recherche, elle, est en log n.

122
00:04:43,990 --> 00:04:45,467
Donc on est prêt à payer quand même

123
00:04:45,567 --> 00:04:46,814
quelque chose qui est de l'ordre

124
00:04:47,103 --> 00:04:48,339
du tri des données,

125
00:04:48,715 --> 00:04:52,024
avant de rechercher.

126
00:04:52,124 --> 00:04:53,608
Alors évidemment, ce type de recherche

127
00:04:53,708 --> 00:04:56,303
a plein, je dirais, d'extensions.

128
00:04:57,653 --> 00:04:59,598
Vous pouvez avoir la recherche par interpolation,

129
00:04:59,698 --> 00:05:00,664
dont j'ai parlé tout à l'heure,

130
00:05:00,983 --> 00:05:02,432
des recherches trichotomiques,

131
00:05:02,532 --> 00:05:04,923
donc ça, c'est par exemple le problème des balances

132
00:05:05,023 --> 00:05:07,199
qu'il y a eu au Capes il y a deux ans,

133
00:05:07,585 --> 00:05:09,252
où vous avez une balance

134
00:05:09,352 --> 00:05:10,332
qui donne trois résultats,

135
00:05:10,432 --> 00:05:11,956
et vous pouvez diviser en trois

136
00:05:12,056 --> 00:05:13,056
votre ensemble d'éléments.

